#!/bin/bash

RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
WHITE=`tput sgr0`

echo ' '

CONFIG_JSON='config.json'
GLOBAL_CSS_FILE=$(cat $CONFIG_JSON | jq '.global_css_file' | tr -d '"')

if [ -d "css" ] 
	then
	rm -R css
	mkdir css
fi

printf '%s\n' '@charset "UTF-8";' > 'sass/compiler.scss'
printf '%s\n' ' ' >> 'sass/compiler.scss'
printf '%s\n' '@import "config/reset.scss";' >> 'sass/compiler.scss'
printf '%s\n' '@import "config/imports.scss";' >> 'sass/compiler.scss'
printf '%s\n' '@import "config/variables.scss";' >> 'sass/compiler.scss'
printf '%s\n' '@import "config/mixins.scss";' >> 'sass/compiler.scss'
printf '%s\n' '@import "config/base.scss";' >> 'sass/compiler.scss'

COUNT=$(cat $CONFIG_JSON | jq '.stylesheets | length')
for((t=0;t<$COUNT;t++)) do
  
	TYPE=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].key' | tr -d '"')
	WRAPPER=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].wrapper' | tr -d '"')

	if [ -d "sass/"$TYPE ] 
	then
		echo "${WHITE}Directory sass/$TYPE already exists."
	else 
		echo "${WHITE}Directory sass/$TYPE does not exist, creating it now..."
		mkdir "sass/"$TYPE
	fi

	printf '%s\n' ' ' >> 'sass/compiler.scss'
	printf '%s\n' '// '$TYPE' Styles //' >> 'sass/compiler.scss'

  	FILES=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].files | length')
	for((i=0;i<$FILES;i++)) do
	  
	  FILE=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].files['${i}'].name' | tr -d '"')
	 	
	  if [ -f "sass/"$TYPE"/"$FILE".scss" ] 
		then
			echo "${WHITE}File sass/$TYPE/$FILE.scss already exists."
		else 
			echo "${WHITE}File sass/$TYPE/$FILE.scss does not exist, creating it now..."
			printf '%s\n' '// '$FILE'.css Styles' >> 'sass/'$TYPE'/'$FILE'.scss'
	  fi

	  if [ $WRAPPER = 'true' ]
	  	then
			printf '%s\n' '.'$TYPE'-'$FILE'{@import "'$TYPE'/'$FILE'.scss";}' >> 'sass/compiler.scss'		
	  else 
	  		printf '%s\n' '@import "'$TYPE'/'$FILE'.scss";' >> 'sass/compiler.scss'
	  fi

	done

	echo ' '

done

sass --watch sass/compiler.scss:css/$GLOBAL_CSS_FILE.css

rm sass/compiler.scss

if ($(cat $CONFIG_JSON | jq '.compile_after_development' | tr -d '"') = 'true')
then
	./production-sass.sh
else
	echo ''
	echo ''
	echo " Nice Work on that CSS! Don't Forget to Compile it for Production By Executing -- production-sass.sh " 
	echo ''
	echo ''
fi	