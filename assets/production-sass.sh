#!/bin/bash

RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
WHITE=`tput sgr0`

echo " "

CONFIG_JSON='config.json'
GLOBAL_CSS_FILE=$(cat $CONFIG_JSON | jq '.global_css_file' | tr -d '"')

# Clean Out CSS Folder
echo "Creating CSS Folder..."
if [ -d "css" ] 
  then
  rm -R css
  mkdir css
  else
  mkdir css
  echo "${GREEN}CSS Folder Successfully Created."
fi

printf '%s\n' '@charset "UTF-8";' > 'sass/global-compiler.scss'
printf '%s\n' '@import "config/reset.scss";' >> 'sass/global-compiler.scss'
printf '%s\n' '@import "config/imports.scss";' >> 'sass/global-compiler.scss'
printf '%s\n' '@import "config/variables.scss";' >> 'sass/global-compiler.scss'
printf '%s\n' '@import "config/mixins.scss";' >> 'sass/global-compiler.scss'
printf '%s\n' '@import "config/base.scss";' >> 'sass/global-compiler.scss'

echo " "

COUNT=$(cat $CONFIG_JSON | jq '.stylesheets | length')
for((t=0;t<$COUNT;t++)) do
  
  TYPE=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].key' | tr -d '"')
  WRAPPER=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].wrapper' | tr -d '"')

  FILES=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].files | length')
  for((i=0;i<$FILES;i++)) do

    if [ -f "sass/compiler.scss" ] 
    then
      rm sass/compiler.scss
    fi

    printf '@charset "UTF-8"; @import "config/variables.scss"; @import "config/mixins.scss";' > 'sass/compiler.scss'
    
    FILE=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].files['${i}'].name' | tr -d '"')
    GLOBAL=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].files['${i}'].global' | tr -d '"')
    MINIMIZE=$(cat $CONFIG_JSON | jq '.stylesheets['${t}'].files['${i}'].minimize' | tr -d '"')
    
    if [ -f "sass/"$TYPE"/"$FILE".scss" ]
    then
      if [ $GLOBAL = 'true' ]
      then
        if [ $WRAPPER = 'true' ]
        then
          printf '%s\n' '.'$TYPE'-'$FILE'{@import "'$TYPE'/'$FILE'.scss";}' >> 'sass/global-compiler.scss'   
        else 
          printf '%s\n' '@import "'$TYPE'/'$FILE'.scss";' >> 'sass/global-compiler.scss'
        fi
      else

        echo "${WHITE}sass/$TYPE/$FILE.scss Successfully Found!"
        
        printf '%s\n' '@import "'$TYPE'/'$FILE'.scss";' >> 'sass/compiler.scss'

        echo "${WHITE}Creating "$TYPE-$FILE".css..."
        sass sass/compiler.scss:css/$TYPE-$FILE.css
        echo "${WHITE}"$TYPE-$FILE".css Successfully Created!"
        rm css/$TYPE-$FILE.css.map

        if [ $MINIMIZE = 'false' ]
        then
          echo "${YELLOW}"$TYPE-$FILE".css not minimized. \"Minimize\" set to \"false\""
        else
          echo "Minifying ${WHITE}"$TYPE-$FILE".css..."
          minify --output css/$TYPE-$FILE.css css/$TYPE-$FILE.css
        fi

        echo " "

      fi
    else
      echo "${RED}sass/"$TYPE"-"$FILE".scss could Not be Found."
      echo " "
    fi

  done

done

echo "${WHITE}Creating "$GLOBAL_CSS_FILE".css"
sass sass/global-compiler.scss:css/$GLOBAL_CSS_FILE.css
echo "${WHITE}"$GLOBAL_CSS_FILE".css Successfully Created!"
echo "${WHITE}Minifying "$GLOBAL_CSS_FILE".css..."
minify --output css/$GLOBAL_CSS_FILE.css css/$GLOBAL_CSS_FILE.css

rm css/$GLOBAL_CSS_FILE.css.map
rm sass/compiler.scss
rm sass/global-compiler.scss

echo ''
echo ''
echo "${WHITE}Your Styles are Ready for Production! Go Treat Yourself to a Cold One!" $'\360\237\215\272'
echo ''
echo ''