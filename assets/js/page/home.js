$(document).ready(function() {

    $(".banner-text").animate({
        opacity: 1
    }, 1500);

    align_banner();

    $(window).resize(function() {
        align_banner();
    });

});

function align_banner() {
    left = (parseInt($(".banner").css("width")) - parseInt($(".banner-text").css("width"))) / 2;
    $(".banner-text").css("left", left + "px");
}