
$(document).ready(function() {

    var current_location = new google.maps.LatLng(45.441349, -75.645888);

    var options = {
        zoom: 15,
        center: current_location,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    }

    var map = new google.maps.Map(document.getElementById("contact-map"), options);

    var marker = new google.maps.Marker({
        map: map,
        position: current_location,
    });

    var infowindow = new google.maps.InfoWindow;

    google.maps.event.addListener(marker, 'click', function() {
        content = '<div id="content"><p><b>Address:</b> 699 Talbot St<br><b>City:</b> London, Ontario<br><b>Phone:</b> (519) 520 8469</b></p></div>';
        infowindow.setContent(content);
        infowindow.open(map, this);
    });
    
});


// if($("#contact-map").length) {  
//         options = {
//             zoom: 13,
//             mapTypeId: google.maps.MapTypeId.ROADMAP,
//             scrollwheel: false
//         }   
            
//         map = new google.maps.Map(document.getElementById("contact-map"), options);     
//         geocoder = new google.maps.Geocoder();
        
//         geocoder.geocode({ 'address': "699 Talbot St London, Ontario"}, function(results, status) { 
//         var marker = new google.maps.Marker({
//             map: map,
//             animation: google.maps.Animation.DROP,
//             position:results[0].geometry.location,
//             icon: base_url+"assets/images/icons/marker.png"
//         });
            
//         var infowindow = new google.maps.InfoWindow({
//                 content : '<div id="content"><p><b>Address:</b> 699 Talbot St<br><b>City:</b> London, Ontario<br><b>Phone:</b> (519) 520 8469</b></p></div>'
//             });
            
//             map.setCenter(marker.getPosition());
//             infowindow.open(map,marker);
            
//             google.maps.event.addListener(marker, 'click', function() {
//                 infowindow.open(map,marker);
//             });
            
//             map.panBy(0, -80);  
//         }); 
//     }