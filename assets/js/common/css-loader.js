// JavaScript Document

window.onload = function () {

	var xhttp;

	if (window.XMLHttpRequest) {
	    	xhttp = new XMLHttpRequest();
	    } else {
	    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xhttp.open("GET", "assets/config.json", false);
	xhttp.send();

	var user_agent = navigator.userAgent;
	var config = JSON.parse(xhttp.responseText);
	var stylesheets = Array();

	user_agent = user_agent.toLowerCase();


	function init() {

		create_comment('');
		create_comment('Available Stylesheets');

		if(config.detect_page) {
			get_page_stylesheets();
		}

		if(config.detect_browser) {
			get_browser_stylesheets();
		}

		if(config.detect_system) {
			get_system_stylesheets();
		}

		if(config.detect_language) {
			get_language_stylesheets();
		}

		create_comment('Available Stylesheets');
		create_comment('');

		for(var i =0; i<stylesheets.length; i++) {
			load_stylesheet(stylesheets[i]);
		}

	}

	function get_page_stylesheets() {
		
		var path = window.location.pathname;
		var params = [];

		if(path[0] == '/') {
			path = path.substr(1);
		}
		if(path[path.length-1] == '/') {
			path = path.substr(0,path.length-1);
		}

		params = path.split('/');

		if(params.length > 0) {
			if(params.length == 1) {
				create_body_class('page-home');
				create_comment('Page: page-home | Filename: page/home.css | Body Class: page-home');
			} else {
				create_body_class('page-'+params[params.length-1]);
				create_comment('Page: '+params[params.length-1]+' | Filename: page/'+params[params.length-1]+'.css | Body Class: page-'+params[params.length-1]);
			}
		}
		
	}

	function get_browser_stylesheets() {

		var browser;
		var browser_version;
		var browser_data = Array();

		browser = user_agent.match(/(opera|chrome|safari|firefox|msie|trident)/g);

		if(browser.length) {
			
			switch (browser[0]) {

				case 'firefox':
					browser_data.push('firefox');
					browser_version = user_agent.match(/firefox\/(\d+(\.\d)*)\./);
					if(browser_version.length) {
						browser_data.push('firefox-'+browser_version[1]);
					}
				break;

				case 'chrome':
					browser_data.push('chrome');
					browser_version = user_agent.match(/chrome\/(\d+(\.\d)*)\./);
					if(browser_version.length) {
						browser_data.push('chrome-'+browser_version[1]);
					}
				break;

				case 'safari':
					browser_data.push('safari');
					browser_version = user_agent.match(/safari\/(\d+(\.\d)*)\./);
					if(browser_version.length) {
						browser_data.push('safari-'+browser_version[1]);
					}
				break;

			}

		}

		for(var i = 0; i<browser_data.length; i++) {
			create_body_class('browser-'+browser_data[i]);
			create_comment('Browser: '+browser_data[i]+' | Filename: browser/'+browser_data[i]+'.css | Body Class: browser-'+browser_data[i]);
			if(in_array(config.stylesheets.browsers,browser_data[i])) {
				stylesheets.push('browser/'+browser_data[i]);
			}
		}

	}

	function get_system_stylesheets() {

		var system;
		var system_version;
		var system_data = Array();

		system = user_agent.match(/(macintosh|windows)/);

		if(system.length) {
			
			switch (system[0]) {

				case 'macintosh':
					system_data.push('macintosh');
					system_version = user_agent.match(/mac os x (\d+(\.\d)*)/);
					if(system_version.length) {
						system_data.push('macintosh-'+system_version[1]);
					}
				break;

			}

		}

		for(var i = 0; i<system_data.length; i++) {
			create_body_class('system-'+system_data[i]);
			create_comment('System: '+system_data[i]+' | Filename: system/'+system_data[i]+'.css | Body Class: system-'+system_data[i]);
			if(in_array(config.systems,system_data[i])) {
				stylesheets.push('system/'+system_data[i]);
			}
		}

	}

	function get_language_stylesheets() {

		var language = document.getElementsByTagName('body')[0].getAttribute('lang');

		if(isset(language) && language != '') {
			create_body_class('lang-'+language);
			create_comment('Languages: '+language+' | Filename: lang/'+language+'.css | Body Class: lang-'+language);
			if(in_array(config.languages,language)) {
				stylesheets.push('language/'+language);
			}
		}

	}

	function load_stylesheet(filename) {

		var stylesheet;
		var filepath = config.css_path+filename;

		if(!config.development) {
			filepath = config.css_path+'min/'+filename+'.min';
		}

		stylesheet = document.createElement('link');
		stylesheet.setAttribute("rel", "stylesheet");
		stylesheet.setAttribute("type", "text/css");
		stylesheet.setAttribute("href", filepath+'.css');

		document.getElementsByTagName("head")[0].appendChild(stylesheet);

	}

	function create_comment(comment) {

		if(config.development) {

			var html = '';

			html = document.createComment(comment);

			document.getElementsByTagName("head")[0].appendChild(html);

		}

	}

	function create_body_class(class_name) {
		if(document.body.className.length) {
			class_name = ' '+class_name;
		}
		document.body.className += class_name;
	}

	function isset(variable) {
		if(typeof(variable) !== undefined) {
			return true;
		}
		return false;
	}

	function in_array(array,value) {
		for(var i=0; i<array.length; i++) {
			if(array[i]==value) {
				return true;
			}
		}
	}

	function array_search(needle,haystack) {
		for(var i = 0; i<haystack.length; i++) {
			if(haystack[i] == needle) {
				return i;
			}
		}
	}

	init();
}
