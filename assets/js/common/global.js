// JavaScript Document

$(document).ready(function(){

	$(window).resize(function() {
		mobile_header();
	});

	mobile_header();

	$(".mobile-button").click(function(){
		if($(".mobile-button").css("background-color") == "rgb(37, 152, 204)") {
			$(".mobile-button").css("background-color","#383838");
			$(".nav-bar").animate({
				height: 0
			},500);
		} else {
			$(".mobile-button").css("background-color","#2598CC");
			$(".nav-bar").animate({
				height: 169
			},500);
		}
	});
});

function mobile_header() {
	if(window.innerWidth<=767) {
		$(".nav-bar").css("height","0px");
	} else {
		$(".nav-bar").css("height","auto");
		$(".mobile-button").css("background-color","#383838");
	}
}
