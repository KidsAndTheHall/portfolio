<?php
class index extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$data['title'] = "Interactive Media Specialist";
		$data['page'] = "home";
		$data['stylesheets'] = [base_url('assets/css/page-home.css')];
		$data['scripts'] = [base_url('assets/js/page/home.js')];
		$data['classes'] = 'page-home';
		$this->load->model("project_model");
		$data['query'] = $this->project_model->projects(null,3);		
		$this->load->view('templates/header',$data);
		$this->load->view('pages/index',$data);
		$this->load->view('templates/footer');
	}
	
	
}
?>