<?php

class Contact extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
		
	public function index() {

		$data['title'] = "Contact";
		$data['page'] = "contact";
		$data['stylesheets'] = ['page-contact'];
		$data['classes'] = 'page-contact';
		$data['scripts'] = ['https://maps.googleapis.com/maps/api/js?v=3&libraries=geometry&sensor=true',base_url('assets/js/page/contact.js')];	

		if($_SERVER['REQUEST_METHOD'] === 'POST') {

			$this->load->model('post_model');
			$this->load->model('mail_model');

			$post_data = $this->post_model->clean();
			
			if($this->mail_model->email($post_data)) {
				$data['email_sent'] = true;
			}

		}
		
		$data['desc'] = "Contact me if you're interested in starting a new project.";	
		$this->load->view('templates/header',$data);
		$this->load->view('pages/contact',$data);	
		$this->load->view('templates/footer');
	}
	
	
}
?>