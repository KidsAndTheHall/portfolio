<?php

class Portfolio extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {

		$data['title'] = "Portfolio";
		$data['page'] = "portfolio";
		$data['desc'] = "Full portfolio of all my projects, including Portions Master, My Rec Hockey, and Strategic Transitions.";

		$this->load->model("project_model");
		$data['query'] = $this->project_model->projects();
				
		$this->load->view('templates/header',$data);
		$this->load->view('pages/portfolio',$data);	
		$this->load->view('templates/footer');
		
	}
		
	
}