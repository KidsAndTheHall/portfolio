<?php

class About extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {

		$data['title'] = "About";
		$data['page'] = "about";
		$data['desc'] = "A little bit about myself.";

		$this->load->view('templates/header',$data);
		$this->load->view('pages/about',$data);	
		$this->load->view('templates/footer');
		
	}
	
}