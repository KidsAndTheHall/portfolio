<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function create_slug($text) { 
    $text = str_replace(" ", "-", $text);
    return $text;
}

function deSlug($text) { 
    $text = str_replace("-", " ", $text);
    return $text;
}