        <footer class="main-footer">
            <h2 class="hidden">Main Footer</h2>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="tagline">
                             Interested in starting a new project?
                             <a href="<?php echo base_url("contact") ?>">Let's talk!</a>
                        </div>
                        <div class="social-buttons">
                            <a href="https://www.facebook.com/KidsAndTheHall" target="_blank">
                                <img src="<?= base_url('assets/images/icons/facebook.png'); ?>" alt="Facebook" title="Facebook" />
                            </a>
                            <a href="https://twitter.com/KidsAndTheHall" target="_blank">
                                <img src="<?= base_url('assets/images/icons/twitter.png'); ?>" alt="Twitter" title="Twitter" />
                            </a>
                            <a href="https://www.linkedin.com/in/alaninteractive" target="_blank">
                                <img src="<?= base_url('assets/images/icons/linkedin.png'); ?>" alt="LinkedIn" title="LinkedIn" />
                            </a>
                            <a href="https://bitbucket.org/KidsAndTheHall/" target="_blank">
                                <img src="<?= base_url('assets/images/icons/bitbucket.png'); ?>" alt="Bitbucket" title="Bitbucket" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>