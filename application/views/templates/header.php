<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width" />
        <title>Alan Stewart <?php if(isset($title)) echo "&there4; ".$title; ?></title>

        <meta name="description" content="<?php if(isset($desc)) { echo $desc; } else { echo $this->config->item('site_desc'); }?>">
        <meta name="keywords" content="<?php $this->config->item('site_keywords'); if(isset($keywords)) echo $keywords; ?>"> 

        <link href="<?= base_url("assets/images/icons/favicon.png") ?>" rel="shortcut icon" type="image/x-icon" />
   
        <link href="<?= base_url("assets/libraries/bootstrap-3.3.6-dist/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />  
        <link href="<?= base_url("assets/css/global.css"); ?>" rel="stylesheet" type="text/css" />   
        <?php 
            if(isset($stylesheets)) {
                foreach($stylesheets as $key) {
                   print '<link rel="stylesheet" type="text/css" href="'.$key.'" />';
                }
            }
        ?>


        <script type="text/javascript" src="<?= base_url("assets/libraries/jquery-3.0.0/jquery-3.0.0.min.js"); ?>"></script>
        <?php 
            if(isset($scripts)) {
                foreach($scripts as $key) {
                   print '<script type="text/javascript" src="'.$key.'"></script>'; 
                }
            }
        ?> 

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-57609639-1', 'auto');
          ga('send', 'pageview');
        </script>

        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="js/html5shiv.js"></script><![endif]-->
    </head>

    <body class="<?= (isset($classes) ? $classes : ''); ?>">

    <header class="main-header">
        <h1 class="hidden"><?= $title ?></h1>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="logo">
                           <a href="<?= base_url(); ?>">
                                <img src="<?= base_url("assets/images/logos/logo.png") ?>" alt="Alan Interactive" title="Alan Stewart" class="logo-max" />
                            </a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <nav class="main-navigation">
                            <h2 class="hidden">Main Navigation</h2>
                            <ul>
                                <li><a href="<?= base_url(); ?>" <?= ($page == 'home' ? "class='active'" : ''); ?>>Home</a></li>
                                <li><a href="<?= base_url("about") ?>" <?= ($page == 'about' ? "class='active'" : ''); ?>>About</a></li>
                                <li><a href="<?= base_url("contact") ?>" <?= ($page == 'contact' ? "class='active'" : ''); ?>>Contact</a></li>
                                <li><a href="<?= base_url("portfolio") ?>" <?= ($page == 'portfolio' ? "class='active'" : ''); ?>>Portfolio</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- <div class="mobile-button">
                <div class="lines">
                    <div class="mobile-line"></div>
                    <div class="mobile-line"></div>
                    <div class="mobile-line"></div>
                </div>
            </div> -->
    </header>
   