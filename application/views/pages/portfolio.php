<section class="main-content">
    <h2 class="hidden">Main Content</h2>

    <div class="container">
    	<?php 

    		$con = 0;

    		foreach($query as $row) {

    			if($con == 0) {
    				print '<div class="row">';
    			}

    			?>

    			<div class="col-md-4">
                    <div class="project-image">
                        <a href="<?= $row->project_link ?>" target='_blank'>
                            <img src="<?= base_url("assets/images/346x200/".$row->project_image) ?>" class='max-img' />
                        </a>
                    </div>
                    <div class="project-title">
                        <a href="<?= $row->project_link ?>">
                            <?= $row->project_name ?>
                        </a>
                    </div>
                    <div class="project-text">
                        <p><?= $row->project_description ?></p>
                    </div>
                </div>

    			<?php

    			$con++;

				if($con == 3) {
					print "</div>";
					$con = 0;
				}	

    		}
    	?>
    </div>

</section>