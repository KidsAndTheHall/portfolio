<section class="main-content">
    <h2 class="hidden">Main Content</h2>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="banner">
                    <div class="banner-text">
                        I Design &amp; Develop Exceptional Modern Websites.
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Latest Projects</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h4>These are just some of the latest projects i've been working on.</h4>
            </div>
        </div>

        <div class="row">
            <?php 
                foreach($query as $row) {
            ?>

                <div class="col-md-4">
                    <div class="project-image">
                        <a href="<?= $row->project_link ?>" target='_blank'>
                            <img src="<?= base_url("assets/images/346x200/".$row->project_image) ?>" class='max-img' />
                        </a>
                    </div>
                    <div class="project-title">
                        <a href="<?= $row->project_link ?>">
                            <?= $row->project_name ?>
                        </a>
                    </div>
                    <div class="project-text">
                        <p><?= $row->project_description ?></p>
                    </div>
                </div>

            <?php
                }
            ?>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="line">
                
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>My Services</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h4>Here are just some of the amazing services I offer.</h4>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <div class="icon-img">
                    <img src="<?= base_url("assets/images/icons/website.png") ?>" />
                </div>
                <div class="project-title">
                    Website Design &amp; Development
                </div>
                <div class="project-text">
                   <p>From large scale content driven websites to small promotional websites, I have years of experience designing and developing websites of various sizes.</p> 
                </div>
            </div>

            <div class="col-md-4">
                <div class="icon-img">
                    <img src="<?= base_url("assets/images/icons/mobile.png") ?>" />
                </div>
                <div class="project-title">
                    Desktop to Mobile Responsive Design
                </div>
                <div class="project-text">
                   <p>I put the power of a full website into the palm of your hand, from desktop to mobile I can make your website fully responsive.</p> 
                </div>
            </div>

            <div class="col-md-4">
                <div class="icon-img">
                    <img src="<?= base_url("assets/images/icons/graphic.png") ?>" />
                </div>
                <div class="project-title">
                    Graphic Design &amp; Branding
                </div>
                <div class="project-text">
                   <p>I have done a variety of graphic design and branding projects. I can create amazing visuals with my experience using Photoshop &amp; Illustrator.</p> 
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-md-4">
                <div class="icon-img">
                    <img src="<?= base_url("assets/images/icons/search.png") ?>" />
                </div>
                <div class="project-title">
                    Search Engine Optimization
                </div>
                <div class="project-text">
                   <p>I know all the tricks to get your site to the top of the search rankings. I'll make sure your website is optimized perfeclty for search engines.</p> 
                </div>
            </div>

            <div class="col-md-4">
                <div class="icon-img">
                    <img src="<?= base_url("assets/images/icons/project.png") ?>" />
                </div>
                <div class="project-title">
                    Project Management
                </div>
                <div class="project-text">
                   <p>Need help organizing a project? I can work with you to find the most efficent way to complete any project on a tight deadline.</p> 
                </div>
            </div>

            <div class="col-md-4">
                <div class="icon-img">
                    <img src="<?= base_url("assets/images/icons/marketing.png") ?>" />
                </div>
                <div class="project-title">
                    Marketing Strategies
                </div>
                <div class="project-text">
                   <p>I strive to create things that are different. Give me the opportunity to catch your audiences' attention with a brand new marketing strategy.</p> 
                </div>
            </div>

        </div>

    </div>

</section>