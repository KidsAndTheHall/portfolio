<section class="main-content">
    <h2 class="hidden">Main Content</h2>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div id="contact-map">
        
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Send me a Message...</h3>
            </div>
        </div>

        <div class="row">
            <form action="<?= base_url("contact") ?>" method="post">
                <div class="col-md-6">
                    <input name="name" placeholder="Name">
                    <input name="subject" placeholder="Subject">
                    <input name="email" placeholder="Email">
                </div>
                <div class="col-md-6">
                    <textarea name="text" placeholder="Write Your Message Here..."></textarea>
                    <input value="Send!" type="submit" name="submit">
                </div>
            </form>
        </div>

    </div>
</section>