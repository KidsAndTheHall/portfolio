<div class="row">
    <div class="grid_3">
        <div class="profile">
            <img src="<?= base_url("assets/images/structure/profile.png"); ?>" />
        </div>
    </div>
	<div class="grid_6">
        <div class="page-title">
            A Little Bit About Myself
        </div>
        <div class="bio">
            <p>First off let me introduce myself, my name is Alan Stewart, I am a web developer originally from Ottawa, Ontario now living in London, Ontario. I have been working for the digital solutions agency Digital Echidna since April 2014 and have made websites in all shapes and sizes.</p>
            <p>Digital Echidna is based out of London, Ontario. We work exclusively with open source technology and focus on building websites using the Drupal content management framework. During my time with Digital Echidna, I developed websites for Businesses throughout Ontario, Cities and Municipalities, the Government, Healthcare Organizations, an International Airport, National Sporting Events, Universities, and the Travel Sector.</p>
            <p>On my own time I develop and maintain a content management system called “MyRecHockey” built on the Codeigniter framework. This CMS provides league conveners with a platform to host their recreational hockey leagues on. Some features within this CMS include complete player statistics and league standings updated automatically daily, full season schedule generation, individual player profiles, and a desktop to mobile responsive design.</p>
            <p>Outside of coding hours. I am very active playing recreational sports including Hockey and Soccer. I am very interested in sports and they influences a lot of my personal projects. Currently I am personally developing a website to track healthy living with a BETA launch date of September 2015.</p>
        </div>
    </div>
    <div class="grid_3">
        <div class="resume">
            <a href="<?= base_url("assets/resume.pdf") ?>" target="_blank">
                <img src="<?= base_url("assets/images/icons/resume.png"); ?>" />
            </a>
        </div>
    </div>
</div>