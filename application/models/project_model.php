<?php

class project_model extends CI_Model {

	public function projects($project_id=null,$limit=null) {

		$this->db->select('*');
		$this->db->from('tbl_project');
		$this->db->order_by('project_date','DESC');

		if(isset($project_id)) {
			$this->db->where('project_id',$project_id);
		}

		if(isset($limit)) {
			$this->db->limit($limit);
		}

		$query = $this->db->get();

		return $query->result();
		
	}
	
}

?>