<?php

class post_model extends CI_Model {

  public function clean() {

    $array = array();
    
    unset($_POST['submit']);
   
    foreach($_POST as $name => $key) {
      $array[$name] = $this->data_alter($name);
    }

    return $array;
    
  }

  public function data_alter($name) {
    return $this->input->post($name,TRUE);
  }

}
