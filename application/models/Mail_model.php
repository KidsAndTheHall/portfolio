<?php

class mail_model extends CI_Model {

  public function email($post_data) {

    $name = $post_data['name'];
    $email = $post_data['email'];
    $subject = $post_data['subject'];
    $message = $post_data['message'];

    $email_message = $message." From:".$name;
    $header = "Reply-To:".$email;

    if(mail(SITE_EMAIL,$subject,$email_message,$header)) {
      return true;
    }

    return false;

  }

}
