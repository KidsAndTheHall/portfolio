<?php
require_once("connect.php");

$collection = $_POST['collection'];
$colour = $_POST['colour'];
$type = $_POST['type'];

$collectionString = "SELECT * FROM tbl_collection ORDER BY collection_name ASC";
$collectionQuery = mysql_query($collectionString);
$collectionRow = mysql_fetch_array($collectionQuery);

$typeString = "SELECT * FROM tbl_type ORDER BY type_name ASC";
$typeQuery = mysql_query($typeString);
$typeRow = mysql_fetch_array($typeQuery);

$colourString = "SELECT * FROM tbl_colour ORDER BY colour_name ASC";
$colourQuery = mysql_query($colourString);
$colourRow = mysql_fetch_array($colourQuery);

if($_POST['collection'] > 0){
	$allProducts = "SELECT * FROM tbl_product, tbl_l_prodCollect, tbl_collection WHERE tbl_product.product_id = tbl_l_prodCollect.product_id AND tbl_collection.collection_id = tbl_l_prodCollect.collection_id AND tbl_collection.collection_id = ".$collection." ORDER BY product_name ASC";
}
else if($_POST['type'] > 0){
	$allProducts = "SELECT * FROM tbl_product, tbl_l_prodType, tbl_type WHERE tbl_product.product_id = tbl_l_prodType.product_id AND tbl_type.type_id = tbl_l_prodType.type_id AND tbl_l_prodType.type_id = ".$type." ORDER BY product_name";
}
else if($_POST['colour'] > 0){
	$allProducts = "SELECT * FROM tbl_product, tbl_l_prodColour, tbl_colour WHERE tbl_product.product_id = tbl_l_prodColour.product_id AND tbl_colour.colour_id = tbl_l_prodColour.colour_id AND tbl_l_prodColour.colour_id = ".$colour." ORDER BY product_name";
}
else if(($_POST['type'] >0)&&($_POST['colour'] >0)){
	$allProducts = "SELECT * FROM tbl_product, tbl_l_prodType, tbl_type, tbl_colour, tbl_l_prodColour, tbl_collection, tbl_l_prodCollect WHERE tbl_product.product_id = tbl_l_prodType.product_id AND tbl_type.type_id = tbl_l_prodType.type_id AND tbl_l_prodType.type_id = ".$type." AND tbl_product.product_id = tbl_l_prodColour.product_id AND tbl_colour.colour_id = tbl_l_prodColour.colour_id AND tbl_l_prodColour.colour_id = ".$colour." ORDER BY product_name";
}
else if(($_POST['colour'] >0)&&($_POST['collection'] >0)){
	$allProducts = "SELECT * FROM tbl_product, tbl_l_prodType, tbl_type, tbl_colour, tbl_l_prodColour, tbl_collection, tbl_l_prodCollect tbl_product.product_id = tbl_l_prodColour.product_id AND tbl_colour.colour_id = tbl_l_prodColour.colour_id AND tbl_l_prodColour.colour_id = ".$colour." AND tbl_product.product_id = tbl_l_prodCollect.product_id AND tbl_collection.collection_id = tbl_l_prodCollect.collection_id AND tbl_l_prodCollect.collection_id = ".$collection." ORDER BY product_name";
}
else if(($_POST['type'] >0)&&($_POST['collection'] >0)){
	$allProducts = "SELECT * FROM tbl_product, tbl_l_prodType, tbl_type, tbl_colour, tbl_l_prodColour, tbl_collection, tbl_l_prodCollect WHERE tbl_product.product_id = tbl_l_prodType.product_id AND tbl_type.type_id = tbl_l_prodType.type_id AND tbl_l_prodType.type_id = ".$type." AND tbl_product.product_id = tbl_l_prodCollect.product_id AND tbl_collection.collection_id = tbl_l_prodCollect.collection_id AND tbl_l_prodCollect.collection_id = ".$collection." ORDER BY product_name";
}
else if(($_POST['type'] >0)&&($_POST['colour'] >0)&&($_POST['collection'] >0)){
	$allProducts = "SELECT * FROM tbl_product, tbl_l_prodType, tbl_type, tbl_colour, tbl_l_prodColour, tbl_collection, tbl_l_prodCollect WHERE tbl_product.product_id = tbl_l_prodType.product_id AND tbl_type.type_id = tbl_l_prodType.type_id AND tbl_l_prodType.type_id = ".$type." AND tbl_product.product_id = tbl_l_prodColour.product_id AND tbl_colour.colour_id = tbl_l_prodColour.colour_id AND tbl_l_prodColour.colour_id = ".$colour." AND tbl_product.product_id = tbl_l_prodCollect.product_id AND tbl_collection.collection_id = tbl_l_prodCollect.collection_id AND tbl_l_prodCollect.collection_id = ".$collection." ORDER BY product_name";
}
else{$allProducts = "SELECT * FROM tbl_product, tbl_l_prodType, tbl_type, tbl_l_prodCollect, tbl_collection WHERE tbl_product.product_id = tbl_l_prodCollect.product_id AND tbl_collection.collection_id = tbl_l_prodCollect.collection_id AND tbl_product.product_id = tbl_l_prodType.product_id AND tbl_type.type_id = tbl_l_prodType.type_id ORDER BY product_name ASC";
}

$results = mysql_query($allProducts);
$numRows = mysql_num_rows($results);
$extra = $numRows%4;
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Madison Handbags</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- 1140px Grid styles for IE -->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" /><![endif]-->

	<!-- The 1140px Grid - http://cssgrid.net/ -->
	<link rel="stylesheet" href="css/1140.css" type="text/css" media="screen" />
	
	<!-- Your styles -->
	<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet' type='text/css'>
	
	<!--css3-mediaqueries-js - http://code.google.com/p/css3-mediaqueries-js/ - Enables media queries in some unsupported browsers-->
	<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
    <script type="text/javascript" src="js/javascript.js"></script>
    <script type="text/javascript" src="js/TweenMax.min.js"></script>
	
</head>


<body onLoad="bannerDrop();">

<div class="container">
	<div class="row">
    	<div class="twocol">
        	<div id="banner">
            	<div id="logo">
                </div>
            	<div id="linkContain">
        			<div id="womens">
                    <p onClick="openWomen();">Womens</p>
                    <ul>
                        <li><a href="#">New At Coach</a></li>
                        <li><a href="#">Spring Looks</a></li>
                        <li><a href="#">Handbags</a></li>
                        <li><a href="#">Wallets</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Watches</a></li>
                        <li><a href="#">Jewlery</a></li>
                    </ul>
                    </div>
                	<div id="mens">
                    <p onClick="openMen();">Mens</p>
                    <ul>
                        <li><a href="#">New At Coach</a></li>
                        <li><a href="#">Spring Looks</a></li>
                        <li><a href="#">Bags</a></li>
                        <li><a href="#">Wallets</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Watches</a></li>
                        <li><a href="#">Belts</a></li>
                    </ul>
                    </div>
                	<div class="shoes"><a onClick="closeAll();" href="#">Shoes</a></div>
                </div>
                <div id="bottomBanner">
                </div>
            </div>
        </div>
        <div class="tencol last">
        	<div class="pageTitle">
            	ALL HANDBAGS
            </div>
        </div>
    </div>
</div>
<div class="filters">
</div>

<div class="container">
	<div class="row">
    	<div class="twocol"></div>
        <div class="sixcol"></div>
        <div class="threecol">
        	<div class="searchContainer">
            <form action="indexFiltered.php" method="post">
            	<div class="searchTitle">
                	Find The Perfect Handbag
                </div>
             	<div class="searchType">
                Colour
                </div>
                <div class="select">
                <select name="colour">
        			<option value="">All Handbag Colours</option>
            			<?php
							while($row=mysql_fetch_array($colourQuery))
							{
								echo "<option value=\"".$row['colour_id']."\">".$row['colour_name']."</option>";
							}
						?>
        		</select>
                </div>
                <div class="searchType">
                Sillhouette
                </div>
                <div class="select">
                <select name="type">
        			<option value="">Select a Handbag Type</option>
            			<?php
							while($row=mysql_fetch_array($typeQuery))
							{
								echo "<option value=\"".$row['type_id']."\">".$row['type_name']."</option>";
							}
						?>
        		</select>
                </div>
                <div class="searchType">
                Collection
                </div>
                <div class="select">
                <select name="collection">
        			<option value="">Select a Handbag Collection</option>
            			<?php
							while($row=mysql_fetch_array($collectionQuery))
							{
								echo "<option value=\"".$row['collection_id']."\">".$row['collection_name']."</option>";
							}
						?>
        		</select>
                </div>
                <div class="searchType">
               Sort by Price
                </div>
                <div class="select">
                <select name="price">
        			<option value="">Sort By Price</option>
                    <option value="">High to Low</option>
                    <option value="">Low to High</option>
            			
        		</select>
                </div>
                <div class="submit">
                	<input type="submit" value="Find Bag!">
                </div>
                </form>
            </div>
        </div>
        <div class="onecol last"></div>
    </div>
	<div class="row">
    	<div class="twocol"></div>
        <div class="tencol last">
        	<?php
            while($row = mysql_fetch_array($results)){
				echo
				"<div class=\"product\">
						<img src=\"images/".$row['product_image']."\">
						<div class=\"name\">".$row['product_name']."</div>
						<div class=\"priceShow\">$".$row['product_price']."</div>
				</div>";
			}
			?>
        </div>
   </div>
</div>
        

</body>
</html>