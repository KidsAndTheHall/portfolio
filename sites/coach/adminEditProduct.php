<?php
require_once("connect.php");
$collectionString = "SELECT * FROM tbl_collection ORDER BY collection_name ASC";
$collectionQuery = mysql_query($collectionString);
$typeString = "SELECT * FROM tbl_type ORDER BY type_name ASC";
$typeQuery = mysql_query($typeString);
$colourString = "SELECT * FROM tbl_colour ORDER BY colour_name ASC";
$colourQuery = mysql_query($colourString);

	$thisone = $_GET['id'];
	$productQuery= "SELECT * FROM tbl_product WHERE tbl_product.product_id=".$thisone."";
	$productResult = mysql_query($productQuery);
	$productRow = mysql_fetch_array($productResult);
	
	$prodCollectQuery= "SELECT * FROM tbl_product, tbl_collection, tbl_l_prodCollect WHERE tbl_product.product_id=".$thisone." AND tbl_l_prodCollect.product_id = tbl_product.product_id AND tbl_l_prodCollect.collection_id = tbl_collection.collection_id";
	$prodCollectResult = mysql_query($prodCollectQuery);
	$prodCollectRow = mysql_fetch_array($prodCollectResult);
	
	$prodTypeQuery= "SELECT * FROM tbl_product, tbl_type, tbl_l_prodType WHERE tbl_product.product_id=".$thisone." AND tbl_l_prodType.product_id = tbl_product.product_id AND tbl_l_prodType.type_id = tbl_type.type_id";
	$prodTypeResult = mysql_query($prodTypeQuery);
	$prodTypeRow = mysql_fetch_array($prodTypeResult);
	
	$prodColourQuery= "SELECT * FROM tbl_product, tbl_colour, tbl_l_prodColour WHERE tbl_product.product_id=".$thisone." AND tbl_l_prodColour.product_id = tbl_product.product_id AND tbl_l_prodColour.colour_id = tbl_colour.colour_id";
	$prodColourResult = mysql_query($prodColourQuery);
	$prodColourRow = mysql_fetch_array($prodColourResult);
	
	
	
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Coach : Handbags</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- 1140px Grid styles for IE -->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" /><![endif]-->

	<!-- The 1140px Grid - http://cssgrid.net/ -->
	<link rel="stylesheet" href="css/1140.css" type="text/css" media="screen" />
	
	<!-- Your styles -->
	<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet' type='text/css'>
	
	<!--css3-mediaqueries-js - http://code.google.com/p/css3-mediaqueries-js/ - Enables media queries in some unsupported browsers-->
	<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
    <script type="text/javascript" src="js/javascript.js"></script>
    <script type="text/javascript" src="js/TweenMax.min.js"></script>
	
</head>


<body onLoad="bannerDrop();">

<div class="container">
	<div class="row">
    	<div class="twocol">
        	<div id="banner">
            	<div id="logo">
                </div>
            	<div id="linkContain">
        			<div id="womens">
                    <p onClick="openWomen();">Womens</p>
                    <ul>
                        <li><a href="#">New At Coach</a></li>
                        <li><a href="#">Spring Looks</a></li>
                        <li><a href="#">Handbags</a></li>
                        <li><a href="#">Wallets</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Watches</a></li>
                        <li><a href="#">Jewlery</a></li>
                    </ul>
                    </div>
                	<div id="mens">
                    <p onClick="openMen();">Mens</p>
                    <ul>
                        <li><a href="#">New At Coach</a></li>
                        <li><a href="#">Spring Looks</a></li>
                        <li><a href="#">Bags</a></li>
                        <li><a href="#">Wallets</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Watches</a></li>
                        <li><a href="#">Belts</a></li>
                    </ul>
                    </div>
                	<div class="shoes"><a onClick="closeAll();" href="#">Shoes</a></div>
                </div>
                <div id="bottomBanner">
                </div>
            </div>
        </div>
        <div class="tencol last">
        	<div class="pageTitle">
            	EDIT HANDBAG
            </div>
        </div>
    </div>
</div>
<div class="filters">
</div>

<div class="container">
	<form action="editProduct.php?product_id=<?php echo $productRow['product_id']?>&collect_id=<?php echo $prodCollectRow['prodCollect_id']?>&type_id=<?php echo $prodTypeRow['prodType_id']?>&colour_id=<?php echo $prodColourRow['prodColour_id']?>" method="post" enctype="multipart/form-data">
    
    <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label class="label">Handbag Name</label></div>
            </div>
            <div class="fivecol">
                   	<input name="name" type="text" value="<?php echo $productRow['product_name'] ?>">                 
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Price</label></div>
            </div>
            <div class="fivecol">
                   	$ <span class="price"><input name="price" type="text" value="<?php echo $productRow['product_price'] ?>"></span>             
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Collection</label></div>
            </div>
            <div class="fivecol">
                   	<select name="collection">
        				<option value="<?php echo $prodCollectRow['collection_id']?>"><?php echo $prodCollectRow['collection_name']?></option>
            				<?php while($row=mysql_fetch_array($collectionQuery))
							{
								echo "<option value=\"".$row['collection_id']."\">".$row['collection_name']."</option>";
							}
							?>
        			</select>               
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Type</label></div>
            </div>
            <div class="fivecol">
                   <select name="type">
        				<option value="<?php echo $prodTypeRow['type_id']?>"><?php echo $prodTypeRow['type_name']?></option>
            				<?php while($row=mysql_fetch_array($typeQuery))
							{
								echo "<option value=\"".$row['type_id']."\">".$row['type_name']."</option>";
							}
							?>
        			</select>          
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Colour</label></div>
            </div>
            <div class="fivecol">
                   <select name="colour">
        				<option value="<?php echo $prodColourRow['colour_id']?>"><?php echo $prodColourRow['colour_name']?></option>
            				<?php while($row=mysql_fetch_array($colourQuery))
							{
								echo "<option value=\"".$row['colour_id']."\">".$row['colour_name']."</option>";
							}
							?>
        			</select>             
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Image</label></div>
            </div>
            <div class="fivecol">
                   	<div id="imageSet">
                        	<input name="imageSet" onClick="showInput()" type="text" value="<?php echo $productRow['product_image']?>">
                        </div>              
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Description</label></div>
            </div>
            <div class="fivecol">
               		<textarea name="description" type="text"><?php echo $productRow['product_description'] ?></textarea>              
            </div>
            <div class="twocol last"></div>
       </div>
    	<div class="row">
    		<div class="twocol"></div>
            <div class="twocol">
           			<input type="submit" value="Save Changes" name="finished">
            </div>
            <div class="eightcol last"></div>
       </div>
        </form>
</div>
</body>
</html>