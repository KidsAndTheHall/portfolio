<?php
require_once("connect.php");
$collectionString = "SELECT * FROM tbl_collection ORDER BY collection_name ASC";
$collectionQuery = mysql_query($collectionString);
$typeString = "SELECT * FROM tbl_type ORDER BY type_name ASC";
$typeQuery = mysql_query($typeString);
$colourString = "SELECT * FROM tbl_colour ORDER BY colour_name ASC";
$colourQuery = mysql_query($colourString);
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Coach : Handbags</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- 1140px Grid styles for IE -->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" /><![endif]-->

	<!-- The 1140px Grid - http://cssgrid.net/ -->
	<link rel="stylesheet" href="css/1140.css" type="text/css" media="screen" />
	
	<!-- Your styles -->
	<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet' type='text/css'>
	
	<!--css3-mediaqueries-js - http://code.google.com/p/css3-mediaqueries-js/ - Enables media queries in some unsupported browsers-->
	<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
    <script type="text/javascript" src="js/javascript.js"></script>
    <script type="text/javascript" src="js/TweenMax.min.js"></script>
	
</head>


<body onLoad="bannerDrop();">

<div class="container">
	<div class="row">
    	<div class="twocol">
        	<div id="banner">
            	<div id="logo">
                </div>
            	<div id="linkContain">
        			<div id="womens">
                    <p onClick="openWomen();">Womens</p>
                    <ul>
                        <li><a href="#">New At Coach</a></li>
                        <li><a href="#">Spring Looks</a></li>
                        <li><a href="#">Handbags</a></li>
                        <li><a href="#">Wallets</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Watches</a></li>
                        <li><a href="#">Jewlery</a></li>
                    </ul>
                    </div>
                	<div id="mens">
                    <p onClick="openMen();">Mens</p>
                    <ul>
                        <li><a href="#">New At Coach</a></li>
                        <li><a href="#">Spring Looks</a></li>
                        <li><a href="#">Bags</a></li>
                        <li><a href="#">Wallets</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Watches</a></li>
                        <li><a href="#">Belts</a></li>
                    </ul>
                    </div>
                	<div class="shoes"><a onClick="closeAll();" href="#">Shoes</a></div>
                </div>
                <div id="bottomBanner">
                </div>
            </div>
        </div>
        <div class="tencol last">
        	<div class="pageTitle">
            	ADD HANDBAG
            </div>
        </div>
    </div>
</div>
<div class="filters">
</div>

<div class="container">
	<div class="row">
    	<div class="twocol"></div>
        	<div class="tencol last">
            	<div class="fail">YOUR ENTRY FAILED TO SUBMIT</div>
            </div>
        </div>
    </div>
	<form action="addProduct.php" method="post" enctype="multipart/form-data">
		<div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Name</label></div>
            </div>
            <div class="fivecol">
                   	<input name="name" type="text">                 
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Price</label></div>
            </div>
            <div class="fivecol">
                   	$ <span class="price"><input name="price" type="text"></span>               
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Collection</label></div>
            </div>
            <div class="fivecol">
                   	<select name="collection">
        				<option value="">Select a Collection Name</option>
            			<?php
							while($row=mysql_fetch_array($collectionQuery))
							{
								echo "<option value=\"".$row['collection_id']."\">".$row['collection_name']."</option>";
							}
						?>
        			</select>                 
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Type</label></div>
            </div>
            <div class="fivecol">
                   <select name="type">
        			<option value="">Select a Handbag Type</option>
            			<?php
							while($row=mysql_fetch_array($typeQuery))
							{
								echo "<option value=\"".$row['type_id']."\">".$row['type_name']."</option>";
							}
						?>
        		</select>             
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Colour</label></div>
            </div>
            <div class="fivecol">
                   <select name="colour">
        			<option value="">Select a Handbag Colour</option>
            			<?php
							while($row=mysql_fetch_array($colourQuery))
							{
								echo "<option value=\"".$row['colour_id']."\">".$row['colour_name']."</option>";
							}
						?>
        		</select>             
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Image</label></div>
            </div>
            <div class="fivecol">
                   	<div class="imageInput">
                    		<input name="image" type="file">
                        </div>                 
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="threecol">
           			<div class="label"><label>Handbag Description</label></div>
            </div>
            <div class="fivecol">
               		<textarea name="description" type="text"></textarea>              
            </div>
            <div class="twocol last"></div>
       </div>
       <div class="row">
    		<div class="twocol"></div>
            <div class="twocol">
           			<input type="submit" value="Save Product" name="finished">
            </div>
            <div class="sixcol">
            	<input type="submit" id="Another" value="Save Product and Add Another" name="another">
            </div>
            <div class="twocol last"></div>
       </div>
        			

        </form>
</div>
</body>
</html>