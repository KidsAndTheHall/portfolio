// JavaScript Document

function bannerDrop()
{
	var banner= document.getElementById("banner");
	TweenMax.to(banner, 1.5,  {css:{top:0}});
}

function showInput(){
	document.getElementById("imageSet").innerHTML = '<input name="image" type="file">';	
}

function openWomen(){
	var womens = document.getElementById("womens");
	TweenMax.to(womens, 1,  {css:{height:204}});
	var mens = document.getElementById("mens");
	TweenMax.to(mens, 1,  {css:{height:32}});
}

function openMen(){
	var womens = document.getElementById("womens");
	TweenMax.to(womens, 1,  {css:{height:32}});
	var mens = document.getElementById("mens");
	TweenMax.to(mens, 1,  {css:{height:204}});
}

function closeAll(){
	var womens = document.getElementById("womens");
	TweenMax.to(womens, 1,  {css:{height:32}});
	var mens = document.getElementById("mens");
	TweenMax.to(mens, 1,  {css:{height:32}});
}