// JavaScript Document

var map, map_canvas, map_options, marker, latlng, service, contentString, infowindow;
var markersArray = [];

function track() {
		navigator.geolocation.getCurrentPosition(showPosition);
		document.getElementById("findLocationAlert").style.display="none";
}

function showPosition(e) {
	var lat = e.coords.latitude;
	var long = e.coords.longitude;
	initialize(lat, long);
	newSearch();
}

function initialize(lat, long) {
	console.log("lat: ", lat, " long: ", long);
	
	latlng = new google.maps.LatLng(lat, long);
	
	mapOptions = {
    	zoom: 11,
    	center: latlng,
    	mapTypeId: google.maps.MapTypeId.SATELLITE
  		}
		
	marker = new google.maps.Marker({
            position: latlng,
            map: map,
			icon: "images/mDkqE.png"
        });
	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	service = new google.maps.places.PlacesService(map);
		
	marker.setMap(map);
}

function newSearch() {
	console.log("search coords: " + latlng);

	var searchString = "Metro Groceries";
	console.log("search for: " + searchString);
	request = {
    	location: latlng,
    	radius: '500',
    	query: searchString
  	};
	
    service.textSearch(request, callback);

    function callback(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
      }

	function createMarker(place) {
		var placeLoc = place.geometry.location;
        var shopMarker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
     });
	  
	markersArray.push(shopMarker);
	
	infowindow = new google.maps.InfoWindow({maxWidth:180});
		google.maps.event.addListener(shopMarker, 'click', function() {
		contentString = '<div class="placeName">' + place.name + '</div></br>' + place.formatted_address + '</br></br>' + '<div class="selectStore" onClick="selectStore();">Select This Metro</div>';
		infowindow.setContent(contentString); 
		infowindow.open(map, this);
    	});
	}
}

function selectStore()
{
	document.getElementById("selectStoreAlert").style.display="block";
}