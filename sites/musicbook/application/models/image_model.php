<?php
class image_model extends CI_Model {
	
public function resize($name, $type, $path, $newWidth, $newHeight, $boxWidth, $boxHeight)
	{
	if($type == "image/png" || $type == "image/jpeg" || $type == "image/gif")
		{	
			$select_image = "../musicbook/assets/images/users/original/".$name;
			
			if($newWidth > 0 || $newHeight > 0)
				{	 
				
					$size = getimagesize($select_image);
					
					if($newWidth > $newHeight)
						{
							$percent = $newWidth / $size[0];	
						}
					else
						{
							$percent = $newHeight / $size[1];	
						}
					
					$width = $size[0]*$percent;
					$height = $size[1]*$percent;
					
					if($boxWidth > 0 || $boxHeight > 0)
						{					
							while($width < $boxWidth || $height < $boxHeight)
								{
									$width=$width*1.5;
									$height=$height*1.5;
								}
								
							$smallerImage = imagecreatetruecolor($boxWidth, $boxHeight);
						}
					else
						{
							$smallerImage = imagecreatetruecolor($width, $height);	
						}
								
												
					if($type == "image/png") // if image type is PNG
						{
							$source = imagecreatefrompng($select_image);
							imagecopyresampled($smallerImage, $source, 0 - (($width-$boxWidth) / 2), 0 - (($height-$boxHeight) / 2), 0, 0, $width, $height, $size[0], $size[1]); 
							imagepng($smallerImage,$path);
						}
					if($type == "image/jpeg") // if image type is PNG
						{
							$source = imagecreatefromjpeg($select_image);
							imagecopyresampled($smallerImage, $source, 0 - (($width-$boxWidth) / 2), 0 - (($height-$boxHeight) / 2), 0, 0, $width, $height, $size[0], $size[1]); 
							imagejpeg($smallerImage,$path); //Creates new image saves to targetpath	
						}
					if($type == "image/gif") // if image type is PNG
						{
							$source = imagecreatefromgif($select_image);
							imagecopyresampled($smallerImage, $source, 0 - (($width-$boxWidth) / 2), 0 - (($height-$boxHeight) / 2), 0, 0, $width, $height, $size[0], $size[1]); 
							imagegif($smallerImage,$path); //Creates new image saves to targetpath	
						}
				}
			}
		}
}
?>