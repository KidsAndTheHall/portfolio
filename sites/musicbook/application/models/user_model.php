<?php

class user_model extends CI_Model {
	
	public function checkLogin() {
		$password = $this->input->post('password');
		$salt = md5($password);
		$hash = sha1($salt.$password);
		
		$query = mysql_query("
		SELECT user_id
		FROM tbl_user 
		WHERE user_email='".$this->input->post('email')."' 
		AND user_password='".$hash."'");
		if(mysql_num_rows($query)==1) {
		$row = mysql_fetch_array($query);
		return $row['user_id'];
		}
	}	
	
	public function findUser($user) {
		$query = mysql_query("
		SELECT user_firstName, user_lastName, user_image, user_location, user_id
		FROM tbl_user 
		WHERE user_id=".$user);
		$row = mysql_fetch_array($query);
		return $row;
	}	
	
	public function new_friends($user) {
		$query = mysql_query("
		SELECT user_firstName, user_lastName, user_id, user_image FROM tbl_user WHERE user_id != '".$user."' LIMIT 0,3
		");	
		return $query;
	}
	
	public function find_friends($user){
		$query = mysql_query("
	SELECT user_firstName, user_lastName, user_image, user_id 
	FROM tbl_user, tbl_l_friends
	WHERE (friends_user = '".$user."'
	AND friends_friend = user_id OR friends_friend = '".$user."' AND friends_user = user_id)
	ORDER BY user_lastName ASC");
	return $query;
	}
	
	
	public function addFriend($user,$friend){
	$query = mysql_query("INSERT INTO tbl_l_friends VALUE(NULL,'".$user."','".$friend."')");
	}
	
	public function removeFriend($user,$friend){
	$query = mysql_query("DELETE FROM tbl_l_friends WHERE (friends_user='".$user."' AND friends_friend='".$friend."') OR (friends_user='".$friend."' AND friends_friend='".$user."')");
	}
	
	public function add_post($user,$wall,$song){
	$text = mysql_real_escape_string($_POST['text']);	
	$query = mysql_query("INSERT INTO tbl_post VALUE(NULL,'".$user."','".$text."','".$song."','".$wall."','".date("Y-m-d")."')");
	}
	
	public function findSongs($user){
	$query = mysql_query("SELECT * FROM tbl_userSong WHERE userSong_user=".$user);
	return $query;	
	}
	
	public function addUser()
	{
		$emailError="";
		$first = mysql_real_escape_string($_POST['first']);
		if($first == NULL) {
		$errors[] = "The first name field was left empty. <br>";
		}

		$last = mysql_real_escape_string($_POST['last']);
		if($last == NULL) {
		$errors[] = "The last name field was left empty. <br>";
		}
		
		$email = mysql_real_escape_string($_POST['email']);
		if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
             $emailError = " The email provided is not a valid email address.";
        }
		$email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
               $emailError = " The email provided is not a valid email address.";
            }
        }
        if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[0])) {
            $domain_array = explode(".", $email_array[0]);
            if (!sizeof($domain_array) >= 2) {
                $emailError = "The Email Provided is Not a Valid Email Address.";
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                     $emailError = "The email provided is not a valid email address.";
                }
            }
		}
	
		
		$password = mysql_real_escape_string($_POST['password']);
		if($password == NULL) {
		$errors[] = "The password field was left empty.<br>";
		}
		
		$location = mysql_real_escape_string($_POST['location']);
		if($location == NULL) {
		$errors[] = "The location field was left empty.<br>";
		}
		
		if(isset($errors)||(isset($emailError)))
		{
		$errmsg = "<b>Please fix the following errors:</b><br>".$emailError."<br>";	
		if(isset($errors))
		{
		$errcount = count($errors);	
		if($errcount > 0) {
		for($i=0; $i< $errcount; $i++) {
		$errmsg .= $errors[$i];
		}
		}
		}
		return $errmsg;
		}
		
		else{
		$salt = md5($password);
		$hash = sha1($salt.$password);
		
	$query = mysql_query("INSERT INTO tbl_user VALUES(NULL,'".$first."','".$last."','".$email."','".$hash."','missingImg.jpg','".$location."','1')");
	$findUser = mysql_query("SELECT user_id FROM tbl_user WHERE user_email='".$email."'");
	$row=mysql_fetch_array($findUser);
	return $row['user_id'];
		}
	}
	
	
	
	public function findPosts($user){
		$query = mysql_query("SELECT post_song, post_text, post_user, user_image, user_firstName, user_lastName, post_date FROM tbl_post, tbl_user WHERE post_wall='".$user."' AND post_user = user_id ORDER BY post_id DESC, post_date DESC");
		return $query;
	}
	
	public function findPostsNoSong($post)
	{
	$query = mysql_query("
	SELECT user_firstName, user_lastName, user_id, post_text, post_date, user_image
	FROM tbl_post, tbl_user
	WHERE post_user = user_id
	AND post_id=".$post);
	$row = mysql_fetch_array($query);	
	return $row;
	}
	
	public function findPostsSong($post)
	{
	$query = mysql_query("
	SELECT user_firstName, user_lastName, user_id, post_text, post_date, user_image, song_name, album_image, album_name, artist_name, song_id, post_wall 
	FROM tbl_post, tbl_user, tbl_song, tbl_album, tbl_artist
	WHERE post_user = user_id
	AND post_song = song_id
	AND song_album = album_id
	AND song_artist = artist_id
	AND post_id=".$post);
	$row = mysql_fetch_array($query);	
	return $row;
	}
	
	public function all_users($user){
	$query = mysql_query("
	SELECT * FROM tbl_user WHERE user_id!='".$user."' ORDER BY user_lastName ASC");
	return $query;	
	}
	
	public function addSong($user,$mp3){
	$query = mysql_query("INSERT INTO tbl_song VALUE(NULL,'".mysql_real_escape_string($_POST['song'])."','".mysql_real_escape_string($mp3)."','".$_POST['artist']."','".$_POST['album']."')");
	}
	
	public function findUserName($user){
	$query = mysql_query("SELECT user_firstName, user_lastName FROM tbl_user WHERE user_id=".$user);
	$row=mysql_fetch_array($query);
	return $row['user_firstName']." ".$row['user_lastName'];	
	}
	
	public function findPlaylist($user){
	$query = mysql_query("
	SELECT song_name, artist_name, song_mp3, album_name, song_id, album_image, song_artist, playlist_user FROM tbl_song, tbl_l_playlist, tbl_artist, tbl_album WHERE playlist_song = song_id AND song_album = album_id AND song_artist = artist_id AND playlist_user=".$user);
	return $query;	
	}
	
	public function playlistArray($user){
	$query=mysql_query("
	SELECT playlist_song FROM tbl_l_playlist WHERE playlist_user=".$user);
	$listArray = array();
	while($row=mysql_fetch_array($query)){
		array_push($listArray,$row['playlist_song']);
		}
		return $listArray;
	}
	
	public function addPlaylist($user,$song){
	$query = mysql_query("INSERT INTO tbl_l_playlist VALUE(NULL,'".$song."','".$user."')");
	}
		
	public function removePlaylist($user,$song){
	$query = mysql_query("DELETE FROM tbl_l_playlist WHERE playlist_user='".$user."' AND playlist_song=".$song);
	}
	
	public function changeStatus($user){
	$query = mysql_query("UPDATE tbl_user SET user_status ='".$_GET['statusID']."' WHERE user_id='".$user."'");
	}
	
	public function findStatus($user){
	$query = mysql_query("SELECT user_status, status_type FROM tbl_user, tbl_status WHERE user_status=status_id AND user_id=".$user);
	$status=mysql_fetch_array($query);
	return $status;	
	}
	
	public function changeImg($user,$image){
	$query = mysql_query("UPDATE tbl_user SET user_image='".$image."' WHERE user_id='".$user."'");	
	}
	
	
	
	
}
?>