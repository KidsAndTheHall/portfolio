<?php
class Login extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index(){
		$data['title'] = 'Sign In';	
		$this->load->view('templates/header',$data);
		$this->load->view('pages/login');
	}
	
	public function create(){
		$data['title'] = 'Create Account';	
		$this->load->view('templates/header',$data);
		$this->load->view('pages/create');
	}
	
	public function loginForm(){
		$this->load->model('user_model');
		$user = $this->user_model->checkLogin();
		if($user>0)
		{
			$data = array(
			'user' => $user,
			'loggedin' => true
			);
			$this->session->set_userdata($data);
			redirect("index");
		}
		else
		{
			$this->index();
		}
	}
	
	public function account(){
		$this->load->model('user_model');
		$user = $this->user_model->addUser();
		if($user>0)
		{
			$data = array(
			'userID' => $user,
			'loggedin' => true
			);
			$this->session->set_userdata($data);
			$errors['text']=$user;	
			$this->load->view('pages/errors',$errors);	
		}
		else
		{
		$errors['text']=$user;	
		$this->load->view('pages/errors',$errors);	
		}
	}
	
	public function signout(){
		$this->session->unset_userdata('data');
		$this->session->sess_destroy();
		$this->index();
	}
		
}
?>