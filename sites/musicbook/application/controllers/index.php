<?php
class index extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		if($this->user()<1)
		{
		redirect('login');	
		}
		
		$data['user'] = $this->user();
		$this->load->vars($data);
	}
	
	public function user(){
	return $this->session->userdata('user');	
	}
	
	public function index($user=0){
		$this->load->model('user_model');
		$this->load->model('artist_model');
		
		if($user==0)
		{
			$user = $this->user();
		}

		$row = $this->user_model->findUser($user);		
		$data['title'] = $row['user_firstName']." ".$row['user_lastName'];
		$data['row'] = $row;
		$data['wallQuery'] = $this->user_model->findPosts($user);	
		
		
		$data['friendQuery'] = $this->user_model->new_friends($user);
		$data['artistQuery']= $this->artist_model->new_artists();
		
		$this->load->view('templates/header',$data);
		$this->load->view('pages/profile',$data);
		$this->load->view('templates/footer');
	}
	
	public function profile($user=0){
		$this->load->model('user_model');
		$this->load->model('artist_model');
		
		if($user==0)
		{
			$user = $this->user();
		}

		$row = $this->user_model->findUser($user);		
		$data['title'] = $row['user_firstName']." ".$row['user_lastName'];
		$data['row'] = $row;
		$data['wallQuery'] = $this->user_model->findPosts($user);	
		
		$data['friendQuery'] = $this->user_model->new_friends($user);
		$data['artistQuery']= $this->artist_model->new_artists();
		
		$this->load->view('templates/header',$data);
		$this->load->view('pages/profile',$data);
		$this->load->view('templates/footer');
	}
	
	public function image(){
		$this->load->model('user_model');
		$this->load->model('artist_model');
	
			$user = $this->user();

		$row = $this->user_model->findUser($user);		
		$data['title'] = "Change Profile Picture";
		$data['row'] = $row;
		$data['wallQuery'] = $this->user_model->findPosts($user);	
		
		$data['friendQuery'] = $this->user_model->new_friends($user);
		$data['artistQuery']= $this->artist_model->new_artists();
		
		$this->load->view('templates/header',$data);
		$this->load->view('pages/image_form',$data);
		$this->load->view('templates/footer');

	}
	
	public function artist($artist)
	{
		$user = $this->user();
		$this->load->model('user_model');
		$this->load->model('artist_model');
		$artist = deSlug($artist);
		
		$row = $this->artist_model->findArtist($artist);
		$data['title'] = $row['artist_name'];
		$data['row'] = $row;	
		$data['albumQuery'] = $this->artist_model->findAlbums($artist);
		$data['friendQuery'] = $this->user_model->new_friends($user);
		$data['artistQuery']= $this->artist_model->new_artists();
		
		$this->load->view('templates/header',$data);
		$this->load->view('pages/artist',$data);
		$this->load->view('templates/footer');
	}
	
	public function change_img(){
	$user=$this->user();
	$this->load->model('user_model');
	$this->load->model('image_model');
	$tmp_name = $_FILES["image"]["tmp_name"];
	$name =  $_FILES["image"]["name"];	
	$type =  $_FILES["image"]["type"];	
	$new_name = strtotime('now').$name;
	$path = "../musicbook/assets/images/users/original/".$new_name;
	if(move_uploaded_file($tmp_name, $path))
	{
	$this->image_model->resize($new_name, $type, "../musicbook/assets/images/users/253x253/".$new_name, 253, 253, 253, 253);
	$this->image_model->resize($new_name, $type, "../musicbook/assets/images/users/150x150/".$new_name, 150, 150, 150, 150);
	$this->image_model->resize($new_name, $type, "../musicbook/assets/images/users/64x64/".$new_name, 64, 64, 64, 64);
	$user=$this->user_model->changeImg($user,$new_name);
	}
	$this->index();
	}
	
	public function friendEntry($friend,$page,$method){
	$user = $this->user();
	$this->load->model('user_model');	
	if($method==1)
	{
	$this->user_model->addFriend($user,$friend);		
	}
	else if($method==2)
	{
	$this->user_model->removeFriend($user,$friend);			
	}
	$this->refreshFriends($page);
	}
	
	public function artistEntry($artist,$page,$method){
	$user = $this->user();
	$this->load->model('artist_model');
	if($method==1)
	{
	$this->artist_model->addLike($user,$artist);		
	}
	else if($method==2)
	{
	$this->artist_model->removeLike($user,$artist);			
	}
	$this->refreshLikes($page);	
	}
	
	public function refreshLikes($page){
		$this->load->model('user_model');
		$user = $this->user();
		$data['artistQuery']= $this->artist_model->new_artists();
		if($page==1)
		{
		$this->load->view('ajax/artists_sidebar',$data);	
		}
		if($page==2)
		{
		$data['artistQuery']= $this->artist_model->all_artists();	
		$this->load->view('ajax/all_artists',$data);	
		}
	}
	
	public function refreshFriends($page){
		$this->load->model('user_model');
		$user = $this->user();
		if($page==2)
		{
		$data['friendQuery']= $this->user_model->new_friends($user);	
		$this->load->view('ajax/friends_sidebar',$data);	
		}
		if($page==1)
		{
		$data['friendQuery']= $this->user_model->all_users($user);	
		$this->load->view('ajax/all_friends',$data);	
		}
	}
	
	public function add_post($wall,$song){
	$user=$this->user();	
	$this->load->model('user_model');
	$this->user_model->add_post($user,$wall,$song);
	$data['wallQuery'] = $this->user_model->findPosts($wall);
	$data['wall'] = $wall;
	$this->load->view('ajax/posts',$data);	
	}
	
	public function findFriends($user){
	$this->load->model('user_model');
	$data['query'] = $this->user_model->find_friends($user);
	$this->load->view('ajax/friends',$data);	
	}
	
	public function findSongs($user){
	$this->load->model('user_model');
	$findSongs['query'] = $this->user_model->findSongs($user);
	$this->load->view('ajax/songs',$findSongs);	
	}
	
	public function findArtists($user){
	$this->load->model('artist_model');
	$data['query'] = $this->artist_model->findArtsitsUser($user);
	$this->load->view('ajax/artists',$data);	
	}
	

	public function findPosts($user){
		$this->load->model('user_model');
		$data['wallQuery'] = $this->user_model->findPosts($user);
		$data['wall'] = $user;
		$this->load->view('ajax/posts',$data);	
	}
	
	public function all_users(){
		$user=$this->user();
		$data['title'] = 'All Users';
		$this->load->model('user_model');	
		$data['friendQuery'] = $this->user_model->all_users($user);
		$this->load->view('templates/header',$data);
		$this->load->view('pages/users',$data);
		$this->load->view('templates/footer');
	}
	
	public function all_artists(){
		$user=$this->user();
		$data['title'] = 'All Artists';
		$this->load->model('artist_model');	
		$data['query']= $this->artist_model->all_artists();
		$this->load->view('templates/header',$data);
		$this->load->view('pages/artists',$data);
		$this->load->view('templates/footer');
	}
	
	public function findPlaylist($user)
	{
		$this->load->model('user_model');
		$playlist['query'] = $this->user_model->findPlaylist($user);
		$playlist['array'] = $this->user_model->playlistArray($user);
		$playlist['wall'] = $user;
		$this->load->view('ajax/playlist',$playlist);
	}
	
	public function playlist_entry($song,$method,$page,$wall,$artist){
	
		$this->load->model('user_model');
		$user=$this->user();
		
		if($method==1)
			{
				$this->user_model->addPlaylist($user,$song);
			}
		
		else if($method==0)
			{
				$this->user_model->removePlaylist($user,$song);	
			}
		
		if($page == 0)
		{
		$data['query'] = $this->user_model->findPlaylist($wall);
		$data['wall'] = $wall;
		$this->load->view('ajax/playlist',$data);
		}
		if($page == 1)
		{
		$this->load->model('artist_model');
		$artist = deSlug($artist);
		$data['wall'] = $wall;
		$data['albumQuery'] = $this->artist_model->findAlbums($artist);
		$this->load->view('ajax/tracklist',$data);
		}
		if($page==2)
		{
		$data['wall'] = $wall;	
		$data['wallQuery'] = $this->user_model->findPosts($wall);
		$this->load->view('ajax/posts',$data);	
		}
	
	}
	
	public function search($keyword=""){
		$this->load->model('artist_model');
		if($keyword!='')
		{
		$data['query'] = $this->artist_model->searchDB($keyword);	
		$this->load->view('ajax/results',$data);
		}
	}
	
	public function load_music($song){
	$this->load->model('music_model');
	$result['row'] = $this->music_model->find_song($song);
	$this->load->view('ajax/player',$result);		
	}

}
?>