<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function tracklist($album,$user){
	$query = mysql_query("SELECT song_name, album_image, album_name, artist_name, song_id, song_artist
FROM tbl_song, tbl_album, tbl_artist 
WHERE song_album = album_id 
AND album_artist = artist_id
AND song_album=".$album);

$string = "<div class=\"albumPlaylist\"><ul>";
	
while($row = mysql_fetch_array($query))
	{
	$string .="
             <li>
             	<div class=\"newsSongImage\" onClick=\"findMusic(".$row['song_id'].")\"><img src=\"".base_url("assets/images/albums/48x48/".$row['album_image'])."\" /></div>
                <div class=\"newsSongName\">".$row['song_name']."</div>
                <div class=\"albumSongArtist\">".$row['artist_name']." - ".$row['album_name']."</div>
				<div class=\"options\">";
				if(check_playlist($row['song_id'],$user))
				{
				$string.="<span class=\"songOptions\" onClick=\"findMusic(".$row['song_id'].")\">Play Song</span> - <span class=\"songOptions\" onClick=\"playlistEntry(".$row['song_id'].",0,1,0,'".createSlug($row['artist_name'])."')\">Remove from Playlist</span></div>";
				}
				else
				{
				$string.="<span class=\"songOptions\" onClick=\"findMusic(".$row['song_id'].")\">Play Song</span> - <span class=\"songOptions\" onClick=\"playlistEntry(".$row['song_id'].",1,1,0,'".createSlug($row['artist_name'])."')\">Add to Playlist</span></div>";	
				}
				
            $string.="</li>";
	}
	$string.="
    </ul>
    </div></div>";	
	
	return $string;
}

function check_playlist($song,$user)
{
$query = mysql_query("SELECT playlist_id FROM tbl_l_playlist WHERE playlist_song='".$song."' AND playlist_user='".$user."'");
if(mysql_num_rows($query)>0)
{
return true;	
}
else
{
return false;	
}
}

function check_friend($user,$friend){
$query = mysql_query("SELECT friends_id FROM tbl_l_friends WHERE (friends_user='".$user."' AND friends_friend='".$friend."' OR friends_user='".$friend."' AND friends_friend ='".$user."')");	
if(mysql_num_rows($query)>0)
{
return true;	
}
else
{
return false;	
}
}

function check_artist($user,$artist){
$query = mysql_query("SELECT like_id FROM tbl_l_like WHERE like_user='".$user."' AND like_artist='".$artist."'");	
if(mysql_num_rows($query)>0)
{
return true;	
}
else
{
return false;	
}
}

function find_song($song,$user,$wall){
$query = mysql_query("
SELECT song_id, album_image, song_name, artist_name, album_name 
FROM tbl_song, tbl_album, tbl_artist
WHERE song_album = album_id
AND album_artist = artist_id
AND song_id =".$song);
$row = mysql_fetch_array($query);

$string ="";
	
	$string.="<div class=\"musicPlaylist\">
            			<ul> 
                			<li>
             	<div class=\"newsSongImage\" onClick=\"findMusic(".$row['song_id'].")\"><img src=\"".base_url("assets/images/albums/48x48/".$row['album_image'])."\" /></div>
                <div class=\"newsSongName\">".$row['song_name']."</div>
                <div class=\"albumSongArtist\">".$row['artist_name']." - ".$row['album_name']."</div>
				<div class=\"options\">";
				if(check_playlist($row['song_id'],$user))
				{
				$string.="<span class=\"songOptions\" onClick=\"findMusic(".$row['song_id'].")\">Play Song</span> - <span class=\"songOptions\" onClick=\"playlistEntry(".$row['song_id'].",0,2,".$wall.",0)\">Remove from Playlist</span></div>";
				}
				else
				{
				$string.="<span class=\"songOptions\" onClick=\"findMusic(".$row['song_id'].")\">Play Song</span> - <span class=\"songOptions\" onClick=\"playlistEntry(".$row['song_id'].",1,2,".$wall.",0)\">Add to Playlist</span></div>";	
				}
				
            $string.="</li>
                		</ul>
            		</div>";
					return $string;
}

function createSlug($text) { 
    $text = str_replace(" ", "-", $text);
    return $text;
}

function deSlug($text) { 
    $text = str_replace("-", " ", $text);
    return $text;
}

