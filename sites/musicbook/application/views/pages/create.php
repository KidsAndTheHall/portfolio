<div class="container">
	<div class="loginBox">
    	<div class="loginLogo">
    		<img src="<?php echo base_url("assets/images/logo.png"); ?>" />
        </div>
        <div class="loginTitle">
        Create an Account
        </div>
	<form onSubmit="return createAccount(this)">
    <div class="name1Input">
    <input name="first" type="text" placeholder="Your First Name">
    </div>
     <div class="name2Input">
    <input name="last" type="text" placeholder="Your Last Name">
    </div>
	<div class="loginInput">
    	<input name="email" type="text" placeholder="Your Email">
        <input name="password" type="password" placeholder="Your Password">
        <input name="location" type="text" placeholder="Your Location">
    </div>
    <div class="submitInput">
    	<input type="submit" value="Create Account">
    </div>
   </form>
    <div class="loginLink">
    <a href="<?php echo base_url("login/login"); ?>">Back to Login</a>
    </div>
    </div>
    <div id="errors"></div>
</div>