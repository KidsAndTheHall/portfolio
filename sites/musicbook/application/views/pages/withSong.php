<?php			
			echo "
			<div class=\"newsBox\">
				<div class=\"newsThumb\">
					<a href=\"".base_url("index.php/profile?userID=".$row['user_id'])."\"><img src=\"".base_url("assets/images/users/64x64/".$row['user_image'])."\" /></a>
				</div>
				<div class=\"newsContent\">
					<a href=\"".base_url("index.php/profile?userID=".$row['user_id'])."\">".$row['user_firstName']." ".$row['user_lastName']."</a> <div class=\"posted\">Posted: ".date("F j, Y", strtotime($row['post_date']))."</div><br>
					<p>".nl2br($row['post_text'])."</p>
					<div class=\"musicPlaylist\">
            			<ul> 
                			<li>
             	<div class=\"newsSongImage\" onClick=\"findMusic(".$row['song_id'].")\"><img src=\"".base_url("assets/images/albums/48x48/".$row['album_image'])."\" /></div>
                <div class=\"newsSongName\">".$row['song_name']."</div>
                <div class=\"albumSongArtist\">".$row['artist_name']." - ".$row['album_name']."</div>
				<div class=\"options\">";
				if(in_array($row['song_id'],$array))
				{
				echo "<span class=\"songOptions\" onClick=\"findMusic(".$row['song_id'].")\">Play Song</span> - <span class=\"songOptions\" onClick=\"playlistEntryUser(".$row['song_id'].",".$row['user_id'].",0,'".$row['post_wall']."')\">Remove from Playlist</span></div>";
				}
				else
				{
				echo "<span class=\"songOptions\" onClick=\"findMusic(".$row['song_id'].")\">Play Song</span> - <span class=\"songOptions\" onClick=\"playlistEntryUser(".$row['song_id'].",".$row['user_id'].",1,'".$row['post_wall']."')\">Add to Playlist</span></div>";	
				}
				
            echo "</li>
                		</ul>
            		</div>
					
        		</div>
			</div>";
?>