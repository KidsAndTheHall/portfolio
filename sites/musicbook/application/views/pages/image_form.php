<div class="container">
	<div class="leftSide">
		<div class="mainImage">
			<img src="<?php echo base_url("assets/images/users/253x253/".$row['user_image']); ?>" />
		</div>
        <nav>
        <h2 class="hidden">Main Navigation</h2>
        <div class="navOptions">
        	<ul>
        		<li><a href="<?php echo base_url() ?>">Home</a></li>
            	<li><a href="<?php echo base_url("index/all_users") ?>">View All Users</a></li>
            	<li><a href="<?php echo base_url("index/all_artists") ?>">View All Artists</a></li>
                <li><a href="<?php echo base_url("index/image") ?>">Change Profile Picture</a></li>
            	<li><a href="<?php echo base_url("login/signout") ?>">Logout</a></li>
        	</ul>
        </div>
        </nav>
    </div>
    <div class="rightSide">
		<div class="name"><?php echo $row['user_firstName']." ".$row['user_lastName'] ?></div>
    	<div class="location"><?php echo $row['user_location'] ?></div>
    <nav>
        <h2 class="hidden">Profile Navigation</h2>
            <div class="nav">
                <ul>
                    <li onClick="findPosts(<?php echo $row['user_id'] ?>);">Wall</li>
                    <li onClick="findFriends(<?php echo $row['user_id'] ?>);">Friends</li>
                    <li onClick="findArtists(<?php echo $row['user_id'] ?>);">Artists</li>
                    <li onClick="findPlaylist(<?php echo $row['user_id'] ?>);">Playlist</li>
                </ul>
            </div>
    </nav>
    <div id="content">
        <?php
echo "<form action=\"".base_url("index/change_img")."\" method=\"post\" enctype=\"multipart/form-data\">
		<div class=\"postBox\">
		<div class=\"uploadTitle\">Change Profile Picture</div>
		<div class=\"formRow\">
		<label>Image File:</label>
		<input type=\"file\" name=\"image\">
		</div>
		</div>
            <input type=\"submit\" value=\"Upload Image\" class=\"submit\">
    </form>";
?>
        </section>
    </div>
    <div class="sidebar">
    <div id="music">
    	<div class="recommendBox">
			<section>
			<h2 class="hidden">Find More Music</h2>
    			<div class="findMore">
    				<a href="<?php echo base_url("index/all_artists") ?>">Find More Music</a>
        		</div>
   				<?php
					while($row = mysql_fetch_array($artistQuery))
						{
			 echo "<div class=\"songBox\">
				<div class=\"recommendImage\">
				<a href=\"".base_url("index/artist/".createSlug($row['artist_name']))."\"><img src=\"".base_url("assets/images/artists/64x64/".$row['artist_image'])."\" /></a>
				</div>
				<div class=\"recommendSong\">
					<a href=\"".base_url("index/artist/".createSlug($row['artist_name']))."\">".$row['artist_name']."</a>
				</div>
				<div class=\"recommendArtist\">";
				if(check_artist($user,$row['artist_id']))
				{
					echo "<div class=\"addFriend\" onClick=\"likeArtist(".$row['artist_id'].",1,2)\">Unlike Artist</div>";
				}
				else
				{
					echo "<div class=\"addFriend\" onClick=\"likeArtist(".$row['artist_id'].",1,1)\">Like Artist</div>";
				}
				echo "</div>
			</div>";
		}
?>
		</section>
    </div>
 </div>
 
 <div id="friends">
 	<div class="recommendBox">
        <section>
        <h2 class="hidden">Find More Friends</h2>
    <div class="findMore">
    		<a href="<?php echo base_url("index/all_users") ?>">Find More Friends</a>
        </div>
    <?php
	while($row = mysql_fetch_array($friendQuery))
	{
		 echo "
<div class=\"songBox\">
	<div class=\"recommendImage\">
    	<a href=\"".base_url("index/profile/".$row['user_id'])."\">
			<img src=\"".base_url("assets/images/users/64x64/".$row['user_image'])."\" />
		</a>
    </div>
    <div class=\"recommendSong\">
        <a href=\"".base_url("index/profile/".$row['user_id'])."\">".$row['user_firstName']." ".$row['user_lastName']."</a>
    </div>
    <div class=\"recommendArtist\">";
			if(check_friend($user,$row['user_id']))
			{
				echo "<div class=\"addFriend\" onClick=\"addFriend(".$row['user_id'].",2,2)\">Remove Friend</div>";
			}
			else
			{
				echo "<div class=\"addFriend\" onClick=\"addFriend(".$row['user_id'].",2,1)\">Add Friend</div>";
			}
            	
            echo"</div>
        </div>";
	}
?>    
</section>
 </div>
 </div>