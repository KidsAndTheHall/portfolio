<div class="container visible">
	<div class="loginBox">
    	<div class="loginLogo">
    		<img src="<?php echo base_url("assets/images/logo.png"); ?>" />
        </div>
        <div class="loginTitle">
        musicbook
        </div>
	<?php echo form_open(base_url('login/loginForm'));?>
	<div class="loginInput">
    	<input name="email" type="text" placeholder="Your Email" value="guest@email.ca"><br>
        <input name="password" type="password" placeholder="Your Password" value="password">
    </div>
    <div class="submitInput">
    	<input type="submit" value="Sign In">
    </div>
    <?php echo form_close(); ?>
    <div class="createLink">
    <a href="<?php echo base_url("login/create"); ?>">Create an Account</a>
    </div>
    <div class="forgetLink">
    <a href="#">Forget Password</a>
    </div>
    </div>
</div>