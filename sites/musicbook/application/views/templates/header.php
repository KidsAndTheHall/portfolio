<!DOCTYPE HTML>
<html>
<head>
	<title>Musicbook &#9835 <?php echo $title ?></title>
 
 <link href="<?php echo base_url("assets/css/reset.css"); ?>" rel="stylesheet" type="text/css" />     
 <link href="<?php echo base_url("assets/css/styles.css"); ?>" rel="stylesheet" type="text/css" /> 
 <link href='http://fonts.googleapis.com/css?family=Arimo:400,700' rel='stylesheet' type='text/css'>
 <link href="<?php echo base_url("assets/images/favicon.png") ?>" rel="shortcut icon" type="image/x-icon" />
 
 <script type="text/javascript" src="<?php echo base_url("assets/js/javascript.js"); ?>"></script>

</head>
<body>
<header>
<h1 class="hidden">Musicbook</h1>
<h2 class="hidden"><?php echo $title ?></h2>
<div class="navBar">
	<div class="container visible">
		<a href="<?php echo base_url(); ?>"><div class="homeButton">
    		<div class="homeLogo">
    			<img src="<?php echo base_url("assets/images/logo.png"); ?>" />
        	</div>
        <div class="homeTitle">
        musicbook
        </div>
    	</div>
        </a>
        <?php
		if(isset($user))
		{
        echo "
		<div id=\"searchBar\">
			<form>
        	<input type=\"search\" onKeyUp=\"showResults(this.value)\" name=\"search\" id=\"search\" autocomplete=\"off\" placeholder=\"Search for Music\">
			<div id=\"suggestions\">
        	</div>
			</form>
        </div>
        <div id=\"nowPlaying\">
			
        </div>";
		}
		?>
   	</div>
</div>

<div class="fixed"></div>
</header>