-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 15, 2013 at 09:06 PM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_musicbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_album`
--

CREATE TABLE `tbl_album` (
  `album_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `album_name` varchar(100) NOT NULL,
  `album_image` varchar(120) NOT NULL,
  `album_artist` mediumint(4) NOT NULL,
  `album_date` date NOT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_album`
--

INSERT INTO `tbl_album` (`album_id`, `album_name`, `album_image`, `album_artist`, `album_date`) VALUES
(1, 'In Your Honor', 'inYourHonor.png', 1, '2005-06-14'),
(2, 'There is Nothing Left to Lose', 'nothingLose.jpg', 1, '1999-11-02'),
(3, 'The Colour and The Shape', 'colour.jpg', 1, '1997-05-07'),
(4, 'Jackson Square', 'jackson.jpg', 2, '2008-10-08'),
(5, 'Michigan Left', 'michigan.jpg', 2, '2011-10-18'),
(6, 'Watch Out!', 'watchout.jpg', 3, '2004-06-29'),
(7, 'Crisis', 'crisis.jpg', 3, '2006-08-22'),
(8, 'Underdogs', 'underdogs.jpg', 4, '1997-10-07'),
(9, 'Beautiful Midnight', 'midnight.jpg', 4, '1999-09-14'),
(10, 'Make Yourself', 'make.jpg', 5, '1999-10-26'),
(11, 'Morning View', 'morning.jpg', 5, '2001-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_artist`
--

CREATE TABLE `tbl_artist` (
  `artist_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `artist_name` varchar(100) NOT NULL,
  `artist_location` varchar(100) NOT NULL,
  `artist_image` varchar(100) NOT NULL,
  PRIMARY KEY (`artist_id`),
  KEY `artist_name` (`artist_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_artist`
--

INSERT INTO `tbl_artist` (`artist_id`, `artist_name`, `artist_location`, `artist_image`) VALUES
(1, 'Foo Fighters', 'Seattle, Washington', 'foofighters.jpg'),
(2, 'Arkells', 'Hamilton, Ontario', 'arkells.jpg'),
(3, 'Alexisonfire', 'St. Catherines, Ontario', 'alexisonfire.jpg'),
(4, 'Matthew Good', 'Vancouver, British Columbia', 'matthewgood.jpg'),
(5, 'Incubus', 'Los Angeles, California', 'incubus.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_l_friends`
--

CREATE TABLE `tbl_l_friends` (
  `friends_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `friends_user` mediumint(4) NOT NULL,
  `friends_friend` mediumint(4) NOT NULL,
  PRIMARY KEY (`friends_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_l_friends`
--

INSERT INTO `tbl_l_friends` (`friends_id`, `friends_user`, `friends_friend`) VALUES
(2, 24, 17),
(7, 24, 2),
(8, 17, 19),
(10, 2, 17),
(11, 2, 35),
(12, 35, 17),
(13, 35, 19),
(14, 19, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_l_like`
--

CREATE TABLE `tbl_l_like` (
  `like_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `like_artist` mediumint(4) NOT NULL,
  `like_user` mediumint(4) NOT NULL,
  PRIMARY KEY (`like_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_l_like`
--

INSERT INTO `tbl_l_like` (`like_id`, `like_artist`, `like_user`) VALUES
(2, 3, 24),
(3, 2, 24),
(7, 1, 24),
(8, 3, 17),
(9, 2, 17),
(11, 5, 35),
(12, 4, 35),
(13, 1, 35),
(14, 2, 19);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_l_playlist`
--

CREATE TABLE `tbl_l_playlist` (
  `playlist_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_song` mediumint(4) NOT NULL,
  `playlist_user` mediumint(4) NOT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `tbl_l_playlist`
--

INSERT INTO `tbl_l_playlist` (`playlist_id`, `playlist_song`, `playlist_user`) VALUES
(1, 1, 24),
(2, 5, 24),
(3, 8, 24),
(4, 24, 24),
(5, 9, 24),
(6, 10, 24),
(7, 11, 24),
(8, 16, 24),
(9, 19, 24),
(10, 25, 24),
(11, 27, 24),
(12, 32, 24),
(13, 33, 24),
(18, 19, 17),
(19, 20, 17),
(20, 16, 17),
(21, 13, 17),
(22, 29, 35),
(23, 27, 35),
(24, 26, 35),
(25, 9, 19),
(26, 11, 19),
(27, 12, 19);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `post_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `post_user` mediumint(4) NOT NULL,
  `post_text` text NOT NULL,
  `post_song` mediumint(4) NOT NULL,
  `post_wall` mediumint(4) NOT NULL,
  `post_date` date NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`post_id`, `post_user`, `post_text`, `post_song`, `post_wall`, `post_date`) VALUES
(1, 24, 'Check out my profile to see how to share music in a post.', 0, 2, '2013-12-15'),
(2, 24, 'To share the song that you''re playing:\n1. See the song below click to play it.\n2. Now that it''s playing you''ll see the "Share What You''re Listening To!" button appear.\n3. Click it and write a post.\n4. Click post.', 1, 24, '2013-12-15'),
(3, 19, 'Hey Alex!', 0, 17, '2013-12-15'),
(4, 19, 'This song is pretty good!', 27, 35, '2013-12-15'),
(5, 24, 'Check this song out.', 19, 17, '2013-12-15'),
(6, 24, 'Hey Brian', 11, 19, '2013-12-15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_song`
--

CREATE TABLE `tbl_song` (
  `song_id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `song_name` varchar(100) NOT NULL,
  `song_mp3` varchar(100) NOT NULL,
  `song_artist` mediumint(4) NOT NULL,
  `song_album` mediumint(4) NOT NULL,
  PRIMARY KEY (`song_id`),
  KEY `song_name` (`song_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `tbl_song`
--

INSERT INTO `tbl_song` (`song_id`, `song_name`, `song_mp3`, `song_artist`, `song_album`) VALUES
(1, 'Best of You', 'bestOfYou.mp3', 1, 1),
(2, 'No Way Back', '', 1, 1),
(3, 'Learn to Fly', '', 1, 2),
(4, 'Breakout', '', 1, 2),
(5, 'Stacked Actors', '', 1, 2),
(6, 'Next Year', '', 1, 2),
(8, 'Everlong', '138698557811 Everlong.m4a', 1, 3),
(9, 'John Lennon', '1387086417John Lennon.mp3', 2, 4),
(10, 'On Paper', '1387086994On Paper.mp3', 2, 5),
(11, 'Where U Going', '1387087079Where U Goin.mp3', 2, 5),
(12, 'Book Club', '1387087095Book Club.mp3', 2, 5),
(13, 'Deadlines', '1387087135Deadlines.mp3', 2, 4),
(14, 'Michigan Left', '1387087188Michigan Left.mp3', 2, 5),
(15, 'I''m Not the Sun', '1387087440I''m Not The Sun.mp3', 2, 4),
(16, 'Side Walks When She Walks', '1387087771Side Walk When She Walks.mp3', 3, 6),
(17, 'That Girl Possessed', '1387087822That Girl Possessed.mp3', 3, 6),
(18, 'It Was Fear of Myself That Made Me Odd', '1387087843It Was Fear Of Myself That Made Me Odd.mp3', 3, 6),
(19, 'This Could be Anywhere in the World', '1387093223', 3, 7),
(20, 'Mailbox Arson', '1387093241', 3, 7),
(21, 'Drunks, Lovers, Sinners, and Saints', '1387093254', 3, 7),
(22, 'DOA', '1387093270', 1, 1),
(23, 'Monkey Wrench', '1387093287', 1, 3),
(24, 'My Hero', '1387093297', 1, 3),
(25, 'Giant', '1387093325', 4, 9),
(26, 'Hello Time Bomb', '1387093337', 4, 9),
(27, 'I Miss New Wave', '1387093352', 4, 9),
(28, 'Apparitions', '1387093376', 4, 8),
(29, 'Everything is Automatic', '1387093392', 4, 8),
(30, 'Drive', '1387093416', 5, 10),
(31, 'Stellar', '1387093422', 5, 10),
(32, 'Pardon Me', '1387093429', 5, 10),
(33, 'Wish You Were Here', '1387093443', 5, 11),
(34, 'Echo', '1387093457', 5, 11),
(35, 'Warning', '1387093475', 5, 11),
(36, 'Mexico', '138713775717 Crimes.mp3', 5, 11);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `status_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `status_type` varchar(10) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`status_id`, `status_type`) VALUES
(1, 'Online'),
(2, 'Offline');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `user_firstName` varchar(50) NOT NULL,
  `user_lastName` varchar(50) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_image` varchar(150) NOT NULL,
  `user_location` varchar(100) NOT NULL,
  `user_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_firstName` (`user_firstName`),
  KEY `user_lastName` (`user_lastName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_firstName`, `user_lastName`, `user_email`, `user_password`, `user_image`, `user_location`, `user_status`) VALUES
(2, 'Paula', 'Weech', 'paula@email.ca', '016abc87842026acd8a48f8e1e1d204abb486947', '1387091666970896_10153087448375705_580675182_n.jpg', 'Victoria, British Columbia', 0),
(17, 'Alex', 'Hague', 'alex@email.ca', '016abc87842026acd8a48f8e1e1d204abb486947', 'alex.jpg', 'Burlington, Ontario', 0),
(19, 'Brian', 'Nuemann', 'b@email.ca', '016abc87842026acd8a48f8e1e1d204abb486947', 'brian.jpg', 'Toronto', 0),
(24, 'Alan', 'Stewart', 'alanstewartsemail@gmail.com', '016abc87842026acd8a48f8e1e1d204abb486947', '13870916211425540_10151741036348601_2010336315_n.jpg', 'Somewhere in Canada', 1),
(35, 'Ryan', 'Cooke', 'ryan@email.ca', '016abc87842026acd8a48f8e1e1d204abb486947', 'ryan.jpg', 'Torotno', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
