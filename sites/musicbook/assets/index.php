<?php
class index extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index(){
	$data['title'] = 'Manage Your Weight the Sensible Way';	
	$this->load->view('templates/header',$data);
	$this->load->view('pages/index');
	
	$this->footer();
	}
	
	public function sidebar(){
	return $this->load->view('templates/sidebar');	
	}
	
	public function footer()
	{
	$this->load->model('blog_model');
	$data['blogQuery'] = $this->blog_model->latestBlogs();	
	return	$this->load->view('templates/footer',$data);
	}
	
	public function products(){		
	$data['title'] = 'Products';	
	$this->load->view('templates/header',$data);
	$this->load->model('product_model');
	$data['query'] = $this->product_model->listProducts();
	$this->load->view('pages/products',$data);
	$this->sidebar();
	$this->footer();	
	}
	
	public function contact($status=0){	
	$data['scripts'] = "loadMap();";	
	$data['title'] = 'Contact';	
	$data['status'] = $status;	
	$this->load->view('templates/header',$data);
	$this->load->view('pages/contact',$data);
	$this->sidebar();
	$this->footer();	
	}
	
	public function weight_loss(){			
	$data['title'] = 'Weight Loss';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/weight_loss',$data);
	$this->sidebar();
	$this->footer();
	}
	
	public function weight_gain(){	
	$data['title'] = 'Weight Gain';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/weight_gain',$data);
	$this->sidebar();
	$this->footer();
	}
	
	public function terms(){			
	$data['title'] = 'Terms &amp; Conditions';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/terms',$data);
	$this->sidebar();
	$this->footer();	
	}
	
	public function healthy_foods(){			
	$data['title'] = 'Health Check';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/healthy_foods');
	$this->sidebar();
	$this->footer();		
	}
	
	public function about(){	
	$data['title'] = 'About Us';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/about');
	$this->sidebar();
	$this->footer();
	}
	
	public function reseller(){
	$data['title'] = 'Reseller Program';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/reseller');
	$this->sidebar();
	$this->footer();	
	}
	
	public function testimonials(){
	$this->load->model('testimony_model');		
	$data['query'] = $this->testimony_model->findTestimonies();		
	$data['title'] = 'Testimonials';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/testimonials',$data);
	$this->sidebar();
	$this->footer();	
	}
	
	public function blogs(){
	$this->load->model('blog_model');	
	$data['query'] = $this->blog_model->findBlogs();	
	$data['title'] = 'From Our Blog';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/blogs',$data);
	$this->sidebar();
	$this->footer();	
	}
	
	public function register(){
	$data['title'] = 'Menu Planner';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/register');
	$this->sidebar();
	$this->footer();	
	}
	
	public function registerEmail(){
	$this->load->model('user_model');
	$this->UserModel->registerEmail();
	$this->index();	
	}
	
	public function blog($blog){
	$this->load->model('blog_model');	
	$data['row'] = $this->blog_model->findBlog($blog);	
	$data['title'] = 'From Our Blog';		
	$this->load->view('templates/header',$data);
	$this->load->view('pages/blog',$data);
	$this->sidebar();
	$this->footer();
	}
	
	public function email(){
	$name = $_POST['name'];
	$email = $_POST['email'];
	$subject = $_POST['subject'];
	$text = $_POST['text'];
	if(isset($_POST['submit']))
	{
	mail("mic@portionsmaster.com",$subject,$text." From: ".$name,"Reply-To: ".$email);
	}
	$this->contact(1);
	}
	
	public function findProduct(){
	$this->load->model('product_model');
	if($_POST['desired'] > 0 && $_POST['current'] > 0)
	{
	$data['query'] = $this->product_model->calculate();		
	}
	else
	{
	$data['query'] = $this->product_model->listProducts();
	}
	$this->load->view('ajax/calculator',$data);	
	}
	public function error404(){
	$data['title'] = 'Unavailable Page';	
	$this->load->view('templates/header',$data);	
	}
	
	public function allProducts(){
	$this->load->model('product_model');	
	$data['query'] = $this->product_model->listProducts();
	$this->load->view('ajax/calculator',$data);		
	}
	
	public function menu_planner()
	{
	$data['title'] = 'Menu Planner';		
	$this->load->view('templates/header',$data);
	$this->load->model('recipe_model');
	$data['query1'] = $this->recipe_model->findWeek(1);	
	$data['query2'] = $this->recipe_model->findWeek(2);	
	$data['query3'] = $this->recipe_model->findWeek(3);	
	$data['query4'] = $this->recipe_model->findWeek(4);	
	$data['query5'] = $this->recipe_model->findWeek(5);		
	$this->load->view('pages/menu_planner',$data);
	$this->sidebar();
	$this->footer();		
	}
	
	public function recipe($recipe)
	{
	$this->load->model('recipe_model');	
	$data['title'] = deSlug($recipe);
	$data['row'] = $this->recipe_model->findRecipe($recipe);		
	$this->load->view('templates/header',$data);	
	$this->load->view('pages/recipe',$data);
	$this->sidebar();
	$this->footer();
	}
	
	public function allRecipes(){
	$this->load->model('recipe_model');	
	$data['query'] = $this->recipe_model->allRecipes();	
	$this->load->view('pages/all_recipes',$data);
	}
	
	
	
}

?>