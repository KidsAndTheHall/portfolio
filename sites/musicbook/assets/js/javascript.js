// JavaScript Document

var xmlHttp;
var share = false;
var base_url = "http://alaninteractive.com/sites/musicbook/"
//var base_url = "http://localhost:8888/alan_interactive/includes/musicbook/"
var nowPlaying = 0;


if(window.XMLHttpRequest){
	xmlHttp = new XMLHttpRequest();	
}
else if(window.ActiveXObject){
	xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
}

function createAccount(thisForm){
	var formData = "";
	
	formData = 
	"first=" + thisForm.elements['first'].value +
	"&last=" + thisForm.elements['last'].value +
	"&email=" + thisForm.elements['email'].value +
	"&password=" + thisForm.elements['password'].value +
	"&location=" + thisForm.elements['location'].value;
	
	
	xmlHttp.onreadystatechange=accountCreated;
	xmlHttp.open("POST",base_url+"login/account",true);
	xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	xmlHttp.setRequestHeader("Content-Length", formData.length);
	xmlHttp.send(formData);
	return false; 
}

function accountCreated(){
if(xmlHttp.readyState==4 || xmlHttp.readyState =="complete")
{
	if(xmlHttp.responseText>0)
	{
	window.location.href = base_url; 
	}
	else
	{
	document.getElementById("errors").innerHTML=xmlHttp.responseText;
	}
}
}

function showResults(str) {
if(xmlHttp==null){
	alert("Browser does not support");	
	return;
}
str = str.split(' ').join('_');
var url=base_url+"index/search/"+str;
xmlHttp.onreadystatechange=stateChanged;
xmlHttp.open("GET",url,true)
xmlHttp.send(null)
}

function stateChanged() { 
if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
	var suggestions = document.getElementById("suggestions");
	suggestions.innerHTML=xmlHttp.responseText;
	if(xmlHttp.responseText!='')
	{
	suggestions.style.display = "block";
	}
	else
	{
	suggestions.style.display = "none";	
	}
}
}

function addFriend(friend,page,method){
	if(xmlHttp==null){alert("Browser does not support");return;}
	xmlHttp.onreadystatechange=refreshFriends;
	xmlHttp.open("GET",base_url+"index/friendEntry/"+friend+"/"+page+"/"+method,true);
	xmlHttp.send(null);
}

function likeArtist(artist,page,method){
	if(xmlHttp==null){alert("Browser does not support");return;}
	xmlHttp.onreadystatechange=refreshMusic;	
	xmlHttp.open("GET",base_url+"index/artistEntry/"+artist+"/"+page+"/"+method,true);
	xmlHttp.send(null);
}

function refreshMusic(){
	if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
	document.getElementById("music").innerHTML=xmlHttp.responseText;
	}
}

function refreshFriends(){
	if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
	document.getElementById("friends").innerHTML=xmlHttp.responseText;
	}
}

function refreshAll(){
	if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
	document.getElementById("users").innerHTML=xmlHttp.responseText;
	}
}



function openImageChange(){
document.getElementById("changePicBox").style.display="block";	
}

function playButton(){
var audio = document.getElementById("audio");
if(audio.paused)
{
audio.play();	
document.getElementById("playButton").style.backgroundImage="url("+base_url+"assets/images/playButton.png)";	
}
else
{
audio.pause();
document.getElementById("playButton").style.backgroundImage="url("+base_url+"assets/images/pauseButton.png)";	
}

}

function newPost(thisForm,i){
	if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	var formData = "";
	
	formData = 
	"text=" + thisForm.elements['text'].value;
	xmlHttp.onreadystatechange=postSuccess;
	if(share==true)
	{
	url=base_url+"index/add_post/"+i+"/"+nowPlaying;
	}
	else
	{
	url=base_url+"index/add_post/"+i+"/0";
	}
	xmlHttp.open("POST",url,true);
	xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	xmlHttp.setRequestHeader("Content-Length",url,formData.length);
	xmlHttp.send(formData);
	return false; 
}

function postSuccess(){
	if(xmlHttp.readyState==4 || xmlHttp.readyState =="complete")
{
	document.getElementById("postForm").reset();
	document.getElementById("content").innerHTML=xmlHttp.responseText;
	if(nowPlaying>0)
	{
		document.getElementById("share").innerHTML = "Share What You're Listening To!";
	}
	if(share==true)
	{
		document.getElementById("share").style.color="#4d7a93";	
	}
	
}
}

function findPosts(i){
	if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	xmlHttp.onreadystatechange=loadPosts;
	xmlHttp.open("GET",base_url+"index/findPosts/"+i,true);
	xmlHttp.send(null);
}

function loadPosts(){
if(xmlHttp.readyState==4 || xmlHttp.readyState =="complete")
{
	document.getElementById("content").innerHTML=xmlHttp.responseText;
	if(nowPlaying>0)
	{
		document.getElementById("share").innerHTML = "Share What You're Listening To!";
	}
	if(share==true)
	{
		document.getElementById("share").style.color="#4d7a93";	
	}
}
}

function findFriends(i){
	if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	xmlHttp.onreadystatechange=fillContent;
	xmlHttp.open("GET",base_url+"index/findFriends/"+i,true);
	xmlHttp.send(null);
}

function findArtists(i){
	if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	xmlHttp.onreadystatechange=fillContent;
	xmlHttp.open("GET",base_url+"index/findArtists/"+i,true);
	xmlHttp.send(null);
}

function findMusic(i){
	if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	xmlHttp.onreadystatechange=loadMusic;
	xmlHttp.open("GET",base_url+"index/load_music/"+i,true);
	xmlHttp.send(null);
	nowPlaying=i;
}

function loadMusic(){
if(xmlHttp.readyState==4 || xmlHttp.readyState =="complete")
{
	document.getElementById("nowPlaying").innerHTML=xmlHttp.responseText;
	audio.play();
	document.getElementById("playButton").style.backgroundImage="url("+base_url+"assets/images/playButton.png)";
	document.getElementById("share").innerHTML = "Share What You're Listening To!";
}
}

function shareMusic()
{
if(share==false)
{	
document.getElementById("share").style.color="#4d7a93";
share=true;
}
else
{
document.getElementById("share").style.color="#999";
share=false;	
}

}

function findPlaylist(i)
{
if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	xmlHttp.onreadystatechange=fillContent;
	xmlHttp.open("GET",base_url+"index/findPlaylist/"+i,true);
	xmlHttp.send(null);	
}

function playlistEntry(song,method,page,user,artist)
{
if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	xmlHttp.onreadystatechange=fillContent;
	xmlHttp.open("GET",base_url+"index/playlist_entry/"+song+"/"+method+"/"+page+"/"+user+"/"+artist,true);
	xmlHttp.send(null);	
}


function fillContent(){
if(xmlHttp.readyState==4 || xmlHttp.readyState =="complete")
{
	document.getElementById("content").innerHTML=xmlHttp.responseText;
	if(nowPlaying>0)
	{
		document.getElementById("share").innerHTML = "Share What You're Listening To!";
	}
	if(share==true)
	{
		document.getElementById("share").style.color="#4d7a93";	
	}
}
}

function loadPage(){
	var url = "";
	urlArray = window.location.pathname.split('/');
	if(urlArray[5] == "artist")
	{
		url = base_url+"index/artist_ajax/"+urlArray[6];
	}
	if(urlArray[4] == "")
	{
		url = base_url+"index/index_ajax";
	}
	if(xmlHttp==null){alert("Browser does not support HTTP Request");}
	xmlHttp.onreadystatechange=fillPage;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);	
}

/*function fillPage(){
	if(xmlHttp.readyState==4 || xmlHttp.readyState =="complete")
	{
		document.getElementById("loadContent").innerHTML=xmlHttp.responseText;
	}
}

function loadHome(){
	history.replaceState(null, null, "index/");
	loadPage();	
}

function loadArtist(artist){
history.pushState(null, null, "index/"+artist);
loadPage();	
}

function change(){
history.pushState(null, null, "hello");
other();
}

function other(){
	history.pushState(null, null, "hello2");
}

window.onload = loadPage();*/
