// JavaScript Document

//var base_url = "http://localhost:8888/strategic_transitions/";
base_url ='http://alaninteractive.com/sites/strategic-transitions';


var map, map_canvas, map_options, marker, latlng, myPosition, image, coordinates, current;
var product_overview, price_shipping, special_features, system_requirements;

var animation = 1000;

$(document).ready(function() {

	hide_sidebar();
	move_logo();
	
	if($(".product-nav").length) {
		element = $("#product-overview").position();
		product_overview = element.top;
		element = $("#price-and-shipping").position();
		product_shipping = element.top;
		element = $("#special-features").position();
		special_features = element.top;
		element = $("#system-requirements").position();
		system_requirements = element.top;
	}

	if($("#contact-map").length){
		myPosition= new google.maps.LatLng(43.999610,  -79.453525);
	
		mapOptions = {
	    	zoom: 15,
	    	center: myPosition,
	    	mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
	  	}	
		map = new google.maps.Map(document.getElementById("contact-map"), mapOptions);
		create_marker();
	};

	if($("#video_player").length){
		height = $("#video_player").css("width");
		height = height.replace("px","");
		$("#video_player").css("height",height*0.6+"px");
	};

	$("#create-account-link").click(function() {
		jQuery.colorbox({
	      inline: true,
	      href: '#login-popup',
	      open: true,
	      width: '500px',
	      height: 'auto',
	      transition: 'fade',
	      speed: 200,
	      opacity: 0.75,
	      fixed: false
	    });
	});

});

$(window).resize(function() {
	adjust_slideshow();
	hide_sidebar();
	move_logo();
});

$(window).scroll(function() {
	if($(".product-nav").length) {
		if(window.pageYOffset >= 215 && window.innerWidth >= 768) {
			$(".sidebar").addClass("fixed-sidebar");
			$(".sidebar-grid").css("height","1px");
			if(window.pageYOffset < product_overview) {
				$(".sidebar a").removeClass("active");
				$(".price-and-shipping").addClass("active");
			} if(window.pageYOffset >= product_overview && window.pageYOffset < special_features) {
				$(".sidebar a").removeClass("active");
				$(".product-overview").addClass("active");
			} if(window.pageYOffset >= special_features && window.pageYOffset < system_requirements) {
				$(".sidebar a").removeClass("active");
				$(".special-features").addClass("active");
			} if(window.pageYOffset >= system_requirements || window.pageYOffset >= ($(document).height()-window.innerHeight)) {
				$(".sidebar a").removeClass("active");
				$(".system-requirements").addClass("active");
			}

		} else {
			$(".sidebar").removeClass("fixed-sidebar");
			$(".sidebar-grid").css("height","auto");
		}
	}
});

function move_logo() {
	if($(".front").length) { 
		if(window.innerWidth < 768) {
			$(".site-logo").prependTo(".intro");
		} else {
			$(".site-logo").prependTo("header .grid_2");
		}
	} else {
		if(window.innerWidth < 768) {
			$(".site-logo").hide();
		} else {
			$(".site-logo").show();
		}
	}
}

function hide_sidebar() {
	if($(".sidebar").length) { 
		$(".sidebar-title").click(function() {
			if(window.innerWidth < 768) {
				if($(".sidebar-content").css("display") == "none") {
					$(".sidebar-content").show();
				} else {
					$(".sidebar-content").hide();
				}
			}
		});
		if(window.innerWidth >= 768) {
			$(".sidebar-content").show();
		} else {
			$(".sidebar-content").hide();
		}
	}
}

function adjust_slideshow() {
	if($("#slideshow").length){
		slides = document.getElementsByClassName("slides");
		timer = setInterval(switch_slides, 8000);
		current = slides.length-1;
		if($("#slideshow").length) {
			$("#slideshow").css("height",slides[0].height+"px");
		}
	};
	
	if($("#slideshow").length) {
		$("#slideshow").css("height",slides[0].height+"px");
	}
	if($("#video_player").length){
		height = $("#video_player").css("width");
		height = height.replace("px","");
		$("#video_player").css("height",height*0.6+"px");
	};
}

function create_marker() {		
  coordinates= new google.maps.LatLng(43.999610, -79.453525);
  var picMarker = new google.maps.Marker({
	  map: map,
	  position: coordinates,
  });

  infowindow = new google.maps.InfoWindow;
	google.maps.event.addListener(picMarker, 'click', function() {
	contentString = '<div id="map-box">Strategic Transitions<br>75 Mary Street, Unit #1<br>Aurora, Ontario<br>L4G 1G3</div>';
	infowindow.setContent(contentString); 
	infowindow.open(map, this);
   });
}


function switch_slides() {
	if(current < 2) {
		current++;
	}
	else {
		current = 0;
	}
	$(slides[current]).animate({opacity:"1"},animation);
	for(i=0; i<=slides.length-1; i++) {
		if(i!=current) {
			$(slides[i]).animate({opacity:"0"},animation);
		}
	}
}