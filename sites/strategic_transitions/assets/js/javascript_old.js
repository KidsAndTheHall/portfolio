// JavaScript Document
//var base_url = "http://localhost:8888/alan_interactive/";
base_url ='http://alaninteractive.com/';

var current;
var animation = 1000;

function slideshow() {
	slides = document.getElementsByClassName("slides");
	timer = setInterval(switch_slides, 8000);
	current = slides.length-1;
	if(slideshow = document.getElementById("slideshow")) {
		slideshow.style.height = slides[current].height+"px";
	}
}

function switch_slides() {
	if(current < 2) {
		current++;
	}
	else {
		current = 0;
	}
	$(slides[current]).animate({opacity:"1"},animation);
	for(i=0; i<=slides.length-1; i++) {
		if(i!=current) {
			$(slides[i]).animate({opacity:"0"},animation);
		}
	}
}

function window_resize() {
	if(slideshow = document.getElementById("slideshow")) {
		slideshow.style.height = slides[current].height+"px";
	}
}

var map, map_canvas, map_options, marker, latlng, myPosition, image, coordinates;

function load_map() {
	myPosition= new google.maps.LatLng(43.999610, -79.453525);
	
	mapOptions = {
    	zoom: 15,
    	center: myPosition,
    	mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false
  		}	
		map = new google.maps.Map(document.getElementById("contact-map"), mapOptions);
		create_maker();
	}
	

function create_maker()
{		
  coordinates= new google.maps.LatLng(43.999610, -79.453525);
  var picMarker = new google.maps.Marker({
	  map: map,
	  position: coordinates,
  });

  infowindow = new google.maps.InfoWindow;
	google.maps.event.addListener(picMarker, 'click', function() {
	contentString = '<div id="mapBox">Portions Master<br>377-532 Montreal Rd<br>Ottawa, ON<br> K1K 4R4</div>';
	infowindow.setContent(contentString); 
	infowindow.open(map, this);
   });
}

window.onresize = window_resize;