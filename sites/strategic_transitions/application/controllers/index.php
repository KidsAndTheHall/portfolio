<?php
class index extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index(){
		$data['active'] = "home";
		$data['classes'] = "front";
		$this->load->view('templates/header',$data);
		$this->load->view('pages/index');
		$this->load->view('templates/footer');
	}

	public function about($page = "company-overview"){	
		$data['page'] = $page;
		$data['active'] = "about";
		$data['title'] = ucwords(str_replace("-"," ",$page));
		$this->load->view('templates/header',$data);
		$this->load->view('templates/breadcrumb',$data);
		$this->load->view('pages/about_sidebar',$data);
		$this->load->view('pages/'.str_replace("-","_",$page),$data);	
		$this->load->view('templates/footer');
	}

	public function contact() {	
		$data['title'] = "Contact Us";
		$data['active'] = "contact";
		$data['scripts'] = '<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&libraries=geometry&sensor=true"></script>';
		$this->load->view('templates/header',$data);
		$this->load->view('templates/breadcrumb',$data);
		$this->load->view('pages/contact',$data);
		$this->load->view('templates/footer');
	}

	public function products($path="") {	
		if($path == "") {
			$type = "all";
			$data['title'] = "Our Products";
			$data['active'] = "products";
			$data['type'] = $type;
			$this->load->model("product_model");
			$data['query'] = $this->product_model->products($type);
			$data['types'] = $this->product_model->product_types();
			$this->load->view('templates/header',$data);
			$this->load->view('templates/breadcrumb',$data);
			$this->load->view('pages/products',$data);
			$this->load->view('templates/footer');
		} else {
		 	$path_array = decode_link($path);
		 	$path_value = explode("=",$path_array);
		 	if($path_value[0] == "view" && isset($path_value[1])) {
				$type = $path_value[1];
				$data['title'] = "Our Products";
				$data['active'] = "products";
				$data['type'] = $type;
				$this->load->model("product_model");
				$data['query'] = $this->product_model->products($type);
				$data['types'] = $this->product_model->product_types();
				$this->load->view('templates/header',$data);
				$this->load->view('templates/breadcrumb',$data);
				$this->load->view('pages/products',$data);
				$this->load->view('templates/footer');
			} else {
				$this->load->model("product_model");
				$row = $this->product_model->product($path);
				$data['title'] = $row['product_name'];
				$data['active'] = "products";
				$data['row'] = $row;
				$data['breadcrumb'] = array(
					"title" => array("Our Products"),
					"path" => array("index/products"),
				);
				$this->load->view('templates/header',$data);
				$this->load->view('templates/breadcrumb',$data);
				$this->load->view('pages/product',$data);
				$this->load->view('templates/footer');
			}
		} 
		
	}

	public function news($path=0){
		$this->load->model("news_model");
		if(is_numeric($path)) {
			$offset = $path;
			$data['title'] = "Recent News";
			$data['active'] = "news";
			$data['offset'] = $offset;
			$data['query'] = $this->news_model->news($offset);
			$data['pages'] = $this->news_model->total_news();
			$this->load->view('templates/header',$data);
			$this->load->view('templates/breadcrumb',$data);
			$this->load->view('pages/news',$data);
			$this->load->view('templates/footer');
		} else {
			$row = $this->news_model->news_item($path);
			$data['row'] = $row;
			$data['title'] = $row['news_title'];
			$data['active'] = "news";
			$data['breadcrumb'] = array(
				"title" => array("Recent News"),
				"path" => array("index/news"),
			);
			$data['query'] = $this->news_model->news(5);
			$this->load->view('templates/header',$data);
			$this->load->view('templates/breadcrumb',$data);
			$this->load->view('pages/news-item',$data);
			$this->load->view('templates/footer');
		}
	}

	public function videos($path=0) {
		$this->load->model("video_model");
		if(is_numeric($path)) {
			$offset = $path;
			$data['title'] = "Video Gallery";
			$data['active'] = "video";
			$data['offset'] = $offset;
			$data['query'] = $this->video_model->videos($offset);
			$data['pages'] = $this->video_model->total_videos();
			$data['archive'] = $this->video_model->video_archive();
			$this->load->view('templates/header',$data);
			$this->load->view('templates/breadcrumb',$data);
			$this->load->view('pages/videos',$data);
			$this->load->view('templates/footer');
		} else {
			$row = $this->video_model->video($path);
			$data['breadcrumb'] = array(
				"title" => array("Videos"),
				"path" => array("index/videos"),
			);
			$data['row'] = $row;
			$data['title'] = $row['video_title'];
			$data['active'] = "video";
			$this->load->view('templates/header',$data);
			$this->load->view('templates/breadcrumb',$data);
			$this->load->view('pages/video',$data);
			$this->load->view('templates/footer');
		}
	}

	public function videos_archive($date) {
		$this->load->model("video_model");
		$data['title'] = "Video Gallery - ".date("F Y",strtotime($date));
		$data['active'] = "video";
		$data['date'] = $date;
		$data['query'] = $this->video_model->videos_by_date($date);
		$data['archive'] = $this->video_model->video_archive();
		$this->load->view('templates/header',$data);
		$this->load->view('templates/breadcrumb',$data);
		$this->load->view('pages/videos',$data);
	}
	
	
	
}
?>