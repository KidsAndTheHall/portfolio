<?php

class video_model extends CI_Model {
	
	public function video($video) {
		if($video=="") {
			$query = mysql_query("SELECT * FROM tbl_videos ORDER BY video_id ASC LIMIT 0,1");
		} else {
			$query = mysql_query("SELECT * FROM tbl_videos WHERE video_title ='".decode_link($video)."'");
		}
		return mysql_fetch_array($query);
	}

	public function videos($offset) {
		$offset = alter_offset($offset,5);
		$query = mysql_query("SELECT * FROM tbl_videos ORDER BY video_date DESC LIMIT ".$offset.",5");
		return $query;
	}

	public function total_videos() {
		$query = mysql_query("SELECT video_id FROM tbl_videos");
		return mysql_num_rows($query);
	}

	public function video_archive() {
		$query = mysql_query("SELECT MONTH(video_date) as months, YEAR(video_date) as years, COUNT(video_date) AS totals FROM tbl_videos GROUP BY years, months ORDER BY years DESC, months DESC");
		return $query;
	}

	public function videos_by_date($date) {
		$query = mysql_query("SELECT * FROM tbl_videos WHERE CONCAT(YEAR(video_date),'-',MONTH(video_date)) = '".$date."'");
		return $query;
	}


}

?>