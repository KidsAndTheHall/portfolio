<div class="row">
	<div class="grid_12">
		<div class="breadcrumb">
			<ul>
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<?php
				if(isset($breadcrumb)) {
					for($i=0; $i<sizeof($breadcrumb['title']); $i++) {
						echo "
						<li>»</li>
						<li><a href=\"".base_url($breadcrumb['path'][$i])."\">".$breadcrumb['title'][$i]."</a></li>";
					}
				}
				?>
				<li>»</li>
				<li class="active"><?php echo $title ?></li>
			</ul>	
		</div>	
	</div>
</div>	