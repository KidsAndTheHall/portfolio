<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width" />
        <meta name="description" content="<?php if(isset($desc)) { echo $desc; } else { echo $this->config->item('site_desc'); }?>">
        <meta name="keywords" content="<?php echo $this->config->item('site_keywords'); if(isset($keywords)) echo $keywords; ?>">

        <title><?php echo $this->config->item('site_name'); if(isset($title)) echo " | ".$title; ?></title>

        <link href="<?php echo base_url("assets/css/reset.css"); ?>" rel="stylesheet" type="text/css" />     
        <link href="<?php echo base_url("assets/css/amazium.css"); ?>" rel="stylesheet" type="text/css" /> 
        <link href="<?php echo base_url("assets/css/base.css"); ?>" rel="stylesheet" type="text/css" /> 
        <link href="<?php echo base_url("assets/css/colorbox.css"); ?>" rel="stylesheet" type="text/css" /> 
        <link href="<?php echo base_url("assets/css/styles.css"); ?>" rel="stylesheet" type="text/css" />    
        <link href="<?php echo base_url("assets/css/layout.css"); ?>" rel="stylesheet" type="text/css" /> 
        <link href="<?php echo base_url("assets/images/favicon.ico") ?>" rel="shortcut icon" type="image/x-icon" />

        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.colorbox.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/javascript.js"); ?>"></script>
        <?php if(isset($scripts)) echo $scripts; ?>

        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="js/html5shiv.js"></script><![endif]-->
    </head>

    <body <?php if(isset($classes)) echo "class='".$classes."'"; ?>>
        <div id="login-popup">
            <form>
                <div class="login">
                    <div class="login-title">
                        Login
                    </div>
                    <input type="text" name="email" placeholder="Your Email">
                    <input type="password" name="password" placeholder="Your Password">
                    <input type="submit" name="submit" value="Login" id="submit">
                    <div class="forget-link">
                        <a href="#">Forget Password</a>
                    </div>    
                </div>
                <div class="create-line">

                </div>  
                <div class="create">
                    <div class="login-title">
                        Create
                    </div>
                    <input type="text" name="first" class="small" placeholder="First Name">
                    <input type="password" name="last" class="small" placeholder="Last Name">
                    <input type="text" name="email" placeholder="Your Email">
                    <input type="password" name="password" placeholder="Your Password">
                    <input type="submit" name="submit" value="Create Account" id="submit-create">
                </div>    
            </form>    
        </div>    
        <header>
            <div class="top-bar">
                <div class="row">
                    <div class="grid_12">
                        <div class="header-phone">Call Toll-free from USA or Canada: 1 (800) 726-7784</div>
                        <div id="create-account-link">Sign In or Create an Account</div>
                     </div>   
                </div>    
            </div>    
            <div class="row">
                <h1 class="hidden"><?php if(isset($title)) { echo $title; } else { echo $this->config->item('site_name'); } ?></h1>
                <div class="grid_2">
                    <div class="site-logo">
                         <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url("assets/images/logo.png") ?>" alt="Strategic Transitions" title="Strategic Transitions" class="max-img" /></a>
                    </div>
                </div>
                <div class="grid_10">    
                    <nav class="main-nav">
                        <h2 class="hidden">Main Navigation</h2>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>" <?php echo check_active($active,"home"); ?>>Home</a></li>
                            <li><a href="<?php echo base_url("index/about"); ?>" <?php echo check_active($active,"about"); ?>>About</a></li>
                            <li><a href="<?php echo base_url("index/contact"); ?>" <?php echo check_active($active,"contact"); ?>>Contact</a></li>
                            <li><a href="<?php echo base_url("index/news"); ?>" <?php echo check_active($active,"news"); ?>>News</a></li>
                            <li><a href="<?php echo base_url("index/products"); ?>" <?php echo check_active($active,"products"); ?>>Products</a></li>
                            <li><a href="<?php echo base_url("index/videos"); ?>" <?php echo check_active($active,"video"); ?>>Videos</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>