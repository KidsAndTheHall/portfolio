<footer>
	<div class="row">
		<div class="grid_3">
			<div class="footer-title">
				Quick Links
			</div>
			<div class="footer-text">
				<div class="footer-col">
					<ul>
			                <li><a href="<?php echo base_url(); ?>">Home</a></li>
			                <li><a href="<?php echo base_url("index/about"); ?>">About</a></li>
			                <li><a href="<?php echo base_url("index/contact"); ?>">Contact</a></li>
					</ul>
				</div>	
				<div class="footer-col">
					<ul>
			                <li><a href="<?php echo base_url("index/news"); ?>">News</a></li>
			                <li><a href="<?php echo base_url("index/products"); ?>">Products</a></li>
			                <li><a href="<?php echo base_url("index/videos"); ?>">Videos</a></li>
					</ul>
				</div>	
			</div>
		</div>
		<div class="grid_3">
			<div class="footer-title">
				Contact Us
			</div>
			<div class="footer-text">
				75 Mary Street, Unit #1<br>
				Aurora, Ontario Canada<br>
				L4G 1G3<br>
				<br>
				Phone: 1-800-726-7784<br>
				Fax: 1-905-726-2856
			</div>		
		</div>
		<div class="grid_3">
			<div class="footer-title">
				Newsletter
			</div>	
			<div class="footer-text">
				Sign up to recieve our monthly newsletter and exclusive offers on featured products.
			</div>	
			<div class="newsletter-form">
				<input type="text" placeholder="Enter Your Email">
				<input type="submit" value="Submit" id="submit">
			</div>
		</div>
		<div class="grid_2">
			<div class="footer-title">
				We're Social
			</div>	
			<ul class="social-icons">
            	<li>
            		<a href="https://www.facebook.com/strategictransitions">
            			<img src="<?php echo base_url("assets/images/icons/social/32-facebook.png"); ?>" alt="Facebook" title="Facebook" />
            		</a>
            	</li>
            	<li>
            		<a href="https://twitter.com/strat_trans">
            			<img src="<?php echo base_url("assets/images/icons/social/32-twitter.png"); ?>" alt="Twitter" title="Twitter" />
            		</a>
            	</li>
            	<li>
            		<a href="http://www.youtube.com/user/StratTrans">
            			<img src="<?php echo base_url("assets/images/icons/social/32-youtube.png"); ?>" alt="Youtube" title="Youtube" />
            		</a>
            	</li>
            	<li>
            		<a href="https://www.linkedin.com/company/strategic-transitions-inc?trk=hb_tab_compy_id_2275358">
            			<img src="<?php echo base_url("assets/images/icons/social/32-linkedin.png"); ?>" alt="LinkedIn" title="LinkedIn" />
            		</a>
            	</li>
			</ul>	
		</div>		
	</div>
</footer>	
<div class="top-bar">
	<div class="row">
		<div class="grid_12">
			<div class="created">Designed &amp; Developed by <a href="http://alaninteractive.com">Alan Stewart</a></div>
		</div>
	</div>
</div>
</body>
</html>