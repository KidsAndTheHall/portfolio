<div class="row">
	<div class="grid_3">
		<div class="sidebar">
			<div class="sidebar-title">
				Video Archive
			</div>
			<div class="sidebar-content">
				<ul>
				<?php
					while($row=mysql_fetch_array($archive)) {
						echo "
						<li>";
							if(isset($date)) {
								if($date == $row['years']."-".$row['months']) {
									echo 
									"<a href=\"".base_url("index/videos_archive/".$row['years']."-".$row['months'])."\" class=\"active\">".
										date("F Y",strtotime($row['years']."-".$row['months']))." (".$row['totals'].")
									</a>";
								} else {
									echo 
									"<a href=\"".base_url("index/videos_archive/".$row['years']."-".$row['months'])."\">".
										date("F Y",strtotime($row['years']."-".$row['months']))." (".$row['totals'].")
									</a>";
								}
							} else {
								echo 
								"<a href=\"".base_url("index/videos_archive/".$row['years']."-".$row['months'])."\">".
									date("F Y",strtotime($row['years']."-".$row['months']))." (".$row['totals'].")
								</a>";
							}
						echo "</li>";
					}
				?>
				</ul>
			</div>
		</div>
	</div>	
	<div class="grid_9">
		<div class="content">
			<div class="page-title">
				<?php echo $title ?>
			</div>
			<div class="content">
				<?php
				while($row=mysql_fetch_array($query)) {
					echo "
					<div class=\"video-wrap\">
						<div class=\"video-img\">
							<a href=\"".base_url("index/videos/".create_link($row['video_title']))."\">
								<img src=\"".base_url("assets/images/320x180/".$row['video_img'])."\" />
							</a>
						</div>
						<div class=\"video-details\">
							<div class=\"video-title\">
								<a href=\"".base_url("index/videos/".create_link($row['video_title']))."\">
									".$row['video_title']."
								</a>
							</div>
							<div class=\"video-date\">
								Posted: ".date("F j, Y",strtotime($row['video_date']))."
							</div>
							<div class=\"video-desc\">
								<p>".$row['video_desc']."</p>
							</div>
							<div class=\"video-link\">
								<a href=\"".base_url("index/videos/".create_link($row['video_title']))."\">
									Play Video
								</a>
							</div>
						</div>
					</div>
					";
				}
				?>
				<?php 
					if(isset($pages) && isset($offset)) {
						echo build_pager("index/videos/",$pages,$offset);
					} 
				?>	
			</div>	
		</div>	
	</div>
</div>	
