<div class="row">
	<div class="grid_3">
		<div class="sidebar">
			<div class="sidebar-title">
				About Us
			</div>
			<div class="sidebar-content">
				<ul>
					<li><a href="<?php echo base_url("index/about"); ?>"<?php if($page=="company-overview") echo " class='active'"; ?>>Company Overview</a></li>
					<li><a href="<?php echo base_url("index/about/our-approach"); ?>"<?php if($page=="our-approach") echo "class='active'"; ?>>Our Approach</a></li>
					<li><a href="<?php echo base_url("index/about/what-we-do"); ?>"<?php if($page=="what-we-do") echo "class='active'"; ?>>What We Do</a></li>
					<li><a href="<?php echo base_url("index/about/our-history"); ?>"<?php if($page=="our-history") echo "class='active'"; ?>>Our History</a></li>
				</ul>
			</div>
		</div>
	</div>