	<div class="grid_7">
		<div class="content">
			<div class="page-title"><?php echo $title ?></div>
			<p>
				Considering the current local/global pace of change and recognizing that there are increasingly fewer secure jobs, businesses, and/or markets, we believe that the best way to prepare one and all for the future is to enhance and accelerate the way we think, learn, understand and express what we know and what we want to say so that we are effectively prepared for the future, regardless of its permutations. 
			</p>
		</div>
	</div>
	<div class="grid_2">
		<img src="<?php echo base_url('assets/images/women_child.png'); ?>" class="max-img side-img" />
	</div>		
</div>	