<div class="row">
	<div class="grid_12">
		<div id="slideshow">
			<img src='<?php echo base_url("assets/images/picture1.png"); ?>' alt="Boy on Computer" title="Boy on Computer" class="max-img slides" />
			<img src='<?php echo base_url("assets/images/picture2.png"); ?>' alt="Boy on Computer" title="Boy on Computer" class="max-img slides" />
			<img src='<?php echo base_url("assets/images/picture3.png"); ?>' alt="Boy on Computer" title="Boy on Computer" class="max-img slides" />
		</div>	
	</div>	
</div>	

<div class="row">
	<div class="intro">
		<div class="grid_8">
			<section>
				<h2 class="page-title">Welcome to Strategic Transitions!</h2>
				<div class="intro-text">
					<p>Strategic Transitions Inc. is an international, visual and digital learning specialist company. 
						We provide state-of-the-art software and learning expertise purposely designed to successfully enhance and accelerate thinking and learning, and make writing easier. 
						Our software and expertise supports students, teachers, parents and individuals from all walks of life, including mainstream and special education, in academic, personal, business, and professional environments.
					Our software and expertise supports students, teachers, parents and individuals from all walks of life, including mainstream and special education, in academic, personal, business, and professional environments.</p>
				</div>	
			</section>
		</div>
		<div class="grid_4">
			<div class="intro-img">
				<img src='<?php echo base_url("assets/images/boy.png"); ?>' alt="Boy on Computer" title="Boy on Computer" class="max-img" />
			</div>
		</div>
	</div>	
</div>	

<div class="row">
	<div class="buttons">
		<div class="grid_4">
			<a href="<?php echo base_url("index/products"); ?>">
				<img src="<?php echo base_url("assets/images/buttons/box.png"); ?>" alt="View Our Products" title="View Our Products" class="max-img" />
			</a>
			<div class="button-title">
				View Our Products
			</div>	
			<div class="button-text">
				State-of-the-art software and learning expertise purposely designed to successfully enhance and accelerate thinking and learning, and make writing easier.
			</div>
		</div>	
		<div class="grid_4">
			<a href="#">
				<img src="<?php echo base_url("assets/images/buttons/iPadBut.png"); ?>" alt="iPad Apps" title="iPad Apps" class="max-img" />
			</a>
			<div class="button-title">
				iPad Apps
			</div>	
			<div class="button-text">
				iWordQ lets you use WordQ on your Mobile Devices. Check this and our other apps only at the Apple App Store.
			</div>
		</div>	
		<div class="grid_4">
			<a href="http://www.prweb.com/releases/prweb8507257.htm">
				<img src="<?php echo base_url("assets/images/buttons/webspirationBut.png"); ?>" alt="Webspiration" title="Webspiration" class="max-img" />
			</a>
			<div class="button-title">
				Webspiration Classroom Service
			</div>	
			<div class="button-text">
				Improve writing and thinking skills online with Webspiration Classroom Service for students and teachers.
			</div>
		</div>	
	</div>
</div>