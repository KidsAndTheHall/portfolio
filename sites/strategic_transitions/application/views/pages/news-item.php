<div class="row">
	<div class="grid_3">
		<div class="sidebar">
			<div class="sidebar-title">
				Upcoming Events
			</div>
			<div class="sidebar-content">
				<ul>
					<li><strong>September 29 - October 1, 2011</strong><br>18 National LD Conference</li>
					<li><strong>October 20 - 21, 2011</strong><br>ECOO</li>
					<li><strong>November 3 - 4, 2011</strong><br>OBIA 2011</li>
					<li><strong>November 16 - 18, 2011</strong><br>ATLE 2011</li>
				</ul>	
			</div>
		</div>	
	</div>
	<div class="grid_7">
		<div class="content">
			<div class="page-title">
				<?php echo $title ?>
				<div class="sub-title">
					Written By <?php echo $row['news_author']; ?> - <?php echo date("F j, Y",strtotime($row['news_date'])) ?>
				</div>	
			</div>
			<?php
				echo "<p>".nl2br($row['news_body'])."</p>";
			?>
		</div>	
	</div>		
</div>	