
<div class="row">
	<div class="grid_3 sidebar-grid">
		<div class="sidebar">
			<div class="sidebar-title">
				Product Details
			</div>
			<div class="sidebar-content">
				<ul class="product-nav">
					<li><a href="#price-and-shipping" class="active price-and-shipping">Price &amp; Shipping</a></li>
					<li><a href="#product-overview" class="product-overview">Product Overview</a></li>
					<li><a href="#special-features" class="special-features">Special Features</a></li>
					<li><a href="#system-requirements" class="system-requirements">System Requirements</a></li>
				</ul>
			</div>	
		</div>	
	</div>
	<div class="grid_9">
		<div class="content">
			<div class="page-title"><?php echo $title ?></div>
			<div class="product-page">
				<div class="product-img">
					<?php echo "<img src=\"".base_url("assets/images/380x420/".$row['product_img'])."\"  />"; ?>
				</div>	
				<div class="details" id="price-and-shipping">
					<div class="price">
						Price: <?php echo $row['product_price'] ?>
					</div>	
					<div class="detail">
						<span class="detail-title">Current Status</span><br><span class="in-stock">In Stock and Ready to Ship</span>
					</div>	
					<div class="detail">
						<span class="detail-title">Shipping Details</span><br>Domestic orders ship may arrive in 3-5 Days International orders may take longer up to 4 weeks.
					</div>
					<div class="detail">
						<a href="#" class="add-to-cart">Add to Cart</a>
					</div>	
				</div>
				<div class="more-details">
					<div class="detail-title" id="product-overview">
						Product Overview
					</div>
					<div class="detail-body">
						<?php 
							if($row['product_desc'] != "") {
								echo "<p>".$row['product_desc']."</p>";
							} else {
								echo "There is no information currently available.";
							}
						?>
					</div>	
					<div class="detail-title" id="special-features">
						Special Features
					</div>
					<div class="detail-body">
						<?php 
							if($row['product_features'] != "") {
								echo "<p>".$row['product_features']."</p>";
							} else {
								echo "There is no information currently available.";
							}
						?>
					</div>	
					<div class="detail-title" id="system-requirements">
						System Requirements
					</div>
					<div class="detail-body">
						<?php 
							if($row['product_windows'] != "") {
								echo "<p>".$row['product_windows']."</p>";
							} else {
								echo "There is no information currently available.";
							}
						?>
					</div>	
				</div>	
			</div>
		</div>	
	</div>		
</div>	