<div class="row">
	<div class="grid_3">
		<div class="sidebar">
			<div class="sidebar-title">
				Video Description
			</div>
			<div class="sidebar-content">
				<div class="sidebar-text">
					<?php echo $row['video_desc'] ?>
				</div>	
			</div>
		</div>
	</div>	
	<div class="grid_9">
		<div class="content">
			<div class="page-title">
				<?php echo $title ?>
			</div>
			<div class="video-box">
				<iframe width="100%" id="video_player" src="<?php echo $row['video_url'] ?>" frameborder="0" allowfullscreen></iframe>
			</div>	
		</div>	
	</div>
</div>	