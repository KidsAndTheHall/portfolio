<div class="row">
	<div class="grid_3">
		<div class="sidebar">
			<div class="sidebar-title">
				Contact Information
			</div>
			<div class="sidebar-content">
				<ul>
					<li><img src="<?php echo base_url("assets/images/icons/house.png"); ?>" class="contact-img contact-house" /><div class="contact-info-first">75 Mary Street, Unit #1<br>Aurora, Ontario Canada<br>L4G 1G3</div></li>
					<li><img src="<?php echo base_url("assets/images/icons/phone.png"); ?>" class="contact-img contact-phone-img" /><div class="contact-info contact-phone">1-800-726-7784</div></li>
					<li><img src="<?php echo base_url("assets/images/icons/fax.png"); ?>" class="contact-img fax-phone-img" /><div class="contact-info contact-phone">1-905-726-2856</div></li>
				</ul>	
			</div>
		</div>	
	</div>
	<div class="grid_7">
		<div class="content">
			<div class="page-title">
				Contact Us
			</div>
			<div id="contact-map">

			</div>
			<form class="contact-form">
				<div class="form-row">
					<label>Name</label>
					<input type="text" name="name" placeholder="Your Name">
				</div>
				<div class="form-row">
					<label>Email</label>
					<input type="text" name="email" placeholder="Your Email">
				</div>
				<div class="form-row">
					<label>Subject</label>
					<input type="text" name="subject" placeholder="Email Subject">
				</div>
				<div class="form-row">
					<label>Message</label>
					<textarea name="text" placeholder="How can we help you?"></textarea>
				</div>
				<div class="form-row">
					<input class="contact-submit" value="Send!">
				</div>	
			</form>	
		</div>	
	</div>		
</div>	