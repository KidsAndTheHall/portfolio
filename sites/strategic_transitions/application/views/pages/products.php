<div class="row">
	<div class="grid_3">
		<div class="sidebar">
			<div class="sidebar-title">
				Find Products
			</div>
			<div class="sidebar-content">
				<ul>
					<li><a href="<?php echo base_url("index/products"); ?>"  <?php if($type == 'all') echo "class='active'"; ?>>All Products</a></li>
					<?php
						while($row = mysql_fetch_array($types)) {
							echo "<li><a href=\"".base_url("index/products/".create_link("view=".$row['category_name']))."\""; if($type == create_link($row['category_name'])) echo "class='active'"; echo ">".$row['category_name']."</a></li>";
						}
					?>
				</ul>
			</div>	
		</div>	
	</div>
	<div class="grid_9">
		<div class="content">
			<div class="page-title"><?php echo $title ?></div>
			<?php
				while($row = mysql_fetch_array($query)) {
					echo "
					<div class=\"product-box\">
						<div class=\"product-img\"><a href=\"".base_url('index/products/'.create_link($row['product_name']))."\"><img src=\"".base_url("assets/images/120x140/".$row['product_img'])."\" /></a></div>
						<div class=\"product-title\"><a href=\"".base_url('index/products/'.create_link($row['product_name']))."\">".$row['product_name']."</a></div>
						<div class=\"product-desc\">".trim_text(120,$row['product_desc'])." <a href=\"".base_url('index/products/'.create_link($row['product_name']))."\">Read More</a></div>

					</div>";
				}
			?>
		</div>	
	</div>		
</div>	