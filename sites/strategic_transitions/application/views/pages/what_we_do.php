	<div class="grid_7">
		<div class="content">
			<div class="page-title"><?php echo $title ?></div>
			<p>
				We provide software, resources, and support for Inspiration, Kidspiration, InspireData, Webspiration Classroom, WordQ, SpeakQ and ABBYY Finereader. We deliver, within one to five business days, worldwide. Our experts and specialists have successfully conceived, developed, and delivered courses and applications involving multiples of up to 500 plus schools and tens of thousands of students. Our software is available worldwide and our learning services have been delivered throughout Canada, as well Bermuda and the United States. 
			</p>
		</div>
	</div>	
	<div class="grid_2">
		<img src="<?php echo base_url('assets/images/girl_on_computer.png'); ?>" class="max-img side-img" />
	</div>	
</div>	