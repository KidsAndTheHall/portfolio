<div class="row">
	<div class="grid_3">
		<div class="sidebar">
			<div class="sidebar-title">
				Upcoming Events
			</div>
			<div class="sidebar-content">
				<ul>
					<li><strong>September 29 - October 1, 2011</strong><br>18 National LD Conference</li>
					<li><strong>October 20 - 21, 2011</strong><br>ECOO</li>
					<li><strong>November 3 - 4, 2011</strong><br>OBIA 2011</li>
					<li><strong>November 16 - 18, 2011</strong><br>ATLE 2011</li>
				</ul>	
			</div>
		</div>	
	</div>
	<div class="grid_7">
		<div class="content">
			<div class="page-title"><?php echo $title ?></div>
			<div class="news-list">
				<ul>
					<?php
						while($row=mysql_fetch_array($query)) {
							echo "
								<li>
									<div class=\"news-title\">
										<a href=\"".base_url("index/news/".create_link($row['news_title']))."\">".$row['news_title']."</a>
									</div>
									<div class=\"news-author\">
										Written By ".$row['news_author']." - ".date("F j, Y",strtotime($row['news_date']))."
									</div>	
									<div class=\"news-body\">
										<p>".trim_text(340,$row['news_body'])." <a href=\"".base_url("index/news/".create_link($row['news_title']))."\">Read More</a></p>
									</div>
								</li>
							";
						}
					?>
				</ul>
			</div>
			<?php echo build_pager("index/news/",$pages,$offset); ?>	
		</div>	
	</div>		
</div>	