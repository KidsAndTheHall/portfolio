	<div class="grid_7">
		<div class="content our-history">
			<div class="page-title"><?php echo $title ?></div>
			<strong>2011</strong>
			<ul>
				<li>Webspiration Classroom: Inspiration Software Inc. appoints Strategic Transitions Inc. as Canadian Distributor for Webspiration Classroom.</li>
				<li>ABBYY FineReader Express Edition: Strategic Transitions Inc. is appointed as a Canadian Reseller for ABBYY software.</li>
				<li>Further global expansion for WordQ™ and SpeakQ™ software with appointment of new resellers in Netherlands, Greece, France and Germany.</li>
			</ul>
			<strong>2010</strong>
			<ul>
				<li>United Kingdom presence established through Assistive Solutions Limited. Ontario Ministry of Education licences WordQ for Ontario’s publicly funded educational institutions.</li>
			</ul>
			<strong>2009</strong>
			<ul>
				<li>ST4 Learning established in New Hampshire to distribute and support software that enhances and accelerates thinking, writing, and learning in educational and professional environments across the United States.</li>
			</ul>
			<strong>2007</strong>
			<ul>
				<li>WordQ and SpeakQ software: Quillsoft Ltd. appoints Strategic Transitions Inc. as Exclusive Global Distributor.</li>
			</ul>
			<strong>1999</strong>
			<ul>
				<li>Inspiration Software: Inspiration Software Inc. appoints Strategic Transitions Inc. as Canadian Distributor, progressively adding Kidspiration and InspireData software.</li>
			</ul>	
			<strong>1997</strong>
			<ul>
				<li>Strategic Transitions Inc.: J. J. Serre, president of J.J. Serre and Associates, Learning and Management consultants, incorporates Strategic Transitions Inc. to distribute and support software that enhances and accelerates thinking, writing, and learning in educational and professional environments.</li>
			</ul>				
		</div>
	</div>
	<div class="grid_2">
		<img src="<?php echo base_url('assets/images/mother_daughter.png'); ?>" class="max-img side-img" />
	</div>	
</div>	