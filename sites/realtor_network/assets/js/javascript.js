// JavaScript Document


//var base_url="http://alaninteractive.com/sites/realtor_network/";
var base_url="http://localhost:8888/alan_interactive/sites/realtor_network/";
var geocoder, address, jsondoc, latLong, map_options, map, coords;


$(document).ready(function(){

	$(window).resize(function(){
		if($("#interactive-map").length) {
			resize_map();
		}
	});

	if($("#interactive-map").length) {
		coords = $("#city").val();
		coords = coords.split(",");
		resize_map();

		$.ajax({
			url:base_url+"index/houses/"+coords[0]
		}).done(function(response) {
			jsondoc = jsonParse(response);
			lat_long = new google.maps.LatLng(coords[1],coords[2]);
			
			map_options = {
				zoom:11 ,
				center: lat_long,
				scrollwheel: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			
			map = new google.maps.Map(document.getElementById("interactive-map"), map_options);
			
			for(var i=0; i<jsondoc.length; i++) {
				$("#properties").append('<div class="imgBox"><div class="houseAddress">'+jsondoc[i].house_address+'</div><a href="'+base_url+'index/property/'+jsondoc[i].house_link+'"><img src="'+base_url+'assets/images/houses/250x164/'+jsondoc[i].house_img+'" /></a></div>');
				createMarker(i);
			}
		});
	}

	if($("#mini-interactive-map").length) {
		url = window.location.pathname.split('/');
		$.ajax({
			url:base_url+"index/house_array/"+url[url.length-2]+"/"+url[url.length-1],
		}).done(function(response) {
			jsondoc = jsonParse(response);
			lat_long = new google.maps.LatLng(jsondoc[0].house_lat,jsondoc[0].house_long);
			
			map_options = {
				zoom:11 ,
				center: lat_long,
				scrollwheel: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			
			map = new google.maps.Map(document.getElementById("mini-interactive-map"), map_options);
			map.panBy(0, -20);	
			createMarker(0);
		});
	}

	for(i=1; i<=4; i++) {
		$("#box-"+i).mouseover(function() {
			$(this).css("background-color","#40ADEB");
			$(this).css("color","#fff");
		});
	}

	$("#box-1").mouseout(function() {
		if($("#select-1").css("height") == "0px") {
			$(this).css("background-color","#fff");
			$(this).css("color","#c2c2c2");
		}
	});
	$("#box-1").click(function() {
		if($("#plus-1").html() == "+") {
			$("#plus-1").html("-");
			$("#select-1").animate({
				height:"55px"
			},200);
			$(this).css("background-color","#40ADEB");
			$(this).css("color","#fff");
		} else {
			$("#plus-1").html("+");
			$("#select-1").animate({
				height:"0px"
			},200, function(){
				$("#box-1").css("background-color","#fff");
				$("#box-1").css("color","#c2c2c2");
			});
		}
	});

	$("#box-2").mouseout(function() {
		if($("#select-2").css("height") == "0px") {
			$(this).css("background-color","#fff");
			$(this).css("color","#c2c2c2");
		}
	});
	$("#box-2").click(function() {
		if($("#plus-2").html() == "+") {
			$("#plus-2").html("-");
			$("#select-2").animate({
				height:"55px"
			},200);
			$(this).css("background-color","#40ADEB");
			$(this).css("color","#fff");
		} else {
			$("#plus-2").html("+");
			$("#select-2").animate({
				height:"0px"
			},200, function(){
				$("#box-2").css("background-color","#fff");
				$("#box-2").css("color","#c2c2c2");
			});
		}
	});

	$("#box-3").mouseout(function() {
		if($("#select-3").css("height") == "0px") {
			$(this).css("background-color","#fff");
			$(this).css("color","#c2c2c2");
		}
	});
	$("#box-3").click(function() {
		if($("#plus-3").html() == "+") {
			$("#plus-3").html("-");
			$("#select-3").animate({
				height:"55px"
			},200);
			$(this).css("background-color","#40ADEB");
			$(this).css("color","#fff");
		} else {
			$("#plus-3").html("+");
			$("#select-3").animate({
				height:"0px"
			},200, function(){
				$("#box-3").css("background-color","#fff");
				$("#box-3").css("color","#c2c2c2");
			});
		}
	});

	$("#box-4").mouseout(function() {
		if($("#select-4").css("height") == "0px") {
			$("#box-4").css("background-color","#fff");
			$("#box-4").css("color","#c2c2c2");
		}
	});
	$("#box-4").click(function() {
		if($("#plus-4").html() == "+") {
			$("#plus-4").html("-");
			$("#select-4").animate({
				height:"55px"
			},200);
			$("#box-3").css("background-color","#40ADEB");
			$("#box-3").css("color","#fff");
		} else {
			$("#plus-4").html("+");
			$("#select-4").animate({
				height:"0px"
			},200, function(){
				$("#box-4").css("background-color","#fff");
				$("#box-4").css("color","#c2c2c2");
			});
		}
	});

	$("#form").submit(function(event) {	
		event.preventDefault();
		$("#properties").html("");
		form = document.getElementById("form");
		var data = 
		"price1=" + form.elements['price1'].value +
		"&price2=" + form.elements['price2'].value +  
		"&bedroom1=" + form.elements['bedroom1'].value + 
		"&bedroom2=" + form.elements['bedroom2'].value + 
		"&bathroom1=" + form.elements['bathroom1'].value + 
		"&bathroom2=" + form.elements['bathroom2'].value +
		"&city=" + form.elements['city'].value;

		$.ajax({
			url:base_url+"index/ajax_houses",
			type:"POST",
			data:data,
		}).done(function(response) {
			coords = $("#city").val();
			coords = coords.split(",");
			jsondoc = jsonParse(response);
			lat_long = new google.maps.LatLng(coords[1],coords[2]);
			
			map_options = {
				zoom:11 ,
				center: lat_long,
				scrollwheel: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			
			map = new google.maps.Map(document.getElementById("interactive-map"), map_options);
			
			for(var i=0; i<jsondoc.length; i++) {
				$("#properties").append('<div class="imgBox"><div class="houseAddress">'+jsondoc[i].house_address+'</div><a href="'+base_url+'index/property/'+jsondoc[i].house_link+'"><img src="'+base_url+'assets/images/houses/250x164/'+jsondoc[i].house_img+'" /></a></div>');
				createMarker(i);
			}
		});
	});

	function createMarker(i) {	
		position = new google.maps.LatLng(jsondoc[i].house_lat, jsondoc[i].house_long);
		var marker = new google.maps.Marker({
			map: map,
			animation: google.maps.Animation.DROP,
			position: position,
			icon: base_url+"assets/images/marker/marker.png"
		});
				
		if($("#interactive-map").length) {
			string = '<div class="mapImg"><img src="'+base_url+'assets/images/houses/175x115/'+jsondoc[i].house_img+'" /></div><div class="mapAddress"><b>'+jsondoc[i].house_address+'</b></div><div class="mapText" style="font-size:10px;"><div class="house-details">Price: $'+numeral(jsondoc[i].house_price).format('0,0')+'<br>Bedrooms: '+jsondoc[i].house_bedroom+'<br>Bathrooms: '+jsondoc[i].house_bathroom+'<br></div>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Contrary to popular belief, Lorem Ipsum is not simply random text.</div><div class="mapLink"><a href="'+base_url+'index/property/'+jsondoc[i].house_link+'"><b>View Full Listing</b></a></div>';
		} else {
			string = '<div><b>'+jsondoc[i].house_address+'</b></div>';
		}

		var infowindow = new google.maps.InfoWindow({
			content:string
		});
				
		google.maps.event.addListener(marker, 'click', function() {
			map.setCenter(marker.getPosition());	
			infowindow.open(map,marker);
		});
	}

	if($(".flexslider").length) {
	 	$('.flexslider').flexslider({
		  animation: "fade",
		  itemMargin: 0,
		  controlNav: false,
		  directionNav: false,
		});
	}

	function resize_map() {
		if($("#interactive-map").length){
			width = $("#interactive-map").css("width");
			width = width.replace("px","");
			height = width*0.45;
			$("#interactive-map").css("height",height+"px");
		}
	}

	if($("#video_player").length){
		height = $("#video_player").css("width");
		height = height.replace("px","");
		$("#video_player").css("height",height*0.6+"px");
	};

});