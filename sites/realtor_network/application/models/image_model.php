<?php
class image_model extends CI_Model {
	
	public function resize($origin, $type, $path, $newWidth, $newHeight, $boxWidth, $boxHeight)
	{
		if($type == "image/png" || $type == "image/jpeg" || $type == "image/gif")
		{	
			if($newWidth > 0 || $newHeight > 0)
			{	 
				$size = getimagesize($origin);	
				
				if($newWidth > $newHeight)
				{
					$percent = $newWidth / $size[0];	
				}
				else
				{
					$percent = $newHeight / $size[1];	
				}
					
				$x = 0;
				$y = 0;
						
				if($boxWidth > 0 || $boxHeight > 0)
				{	
					$width = $size[0]*$percent;
					$height = $size[1]*$percent;				
					while($width < $boxWidth || $height < $boxHeight)
					{
						$width=$width*1.5;
						$height=$height*1.5;
					}
					if($boxWidth > 0)
					{
						$x =  0 -(($width-$boxWidth) / 2);	
					}
					if($boxHeight > 0)
					{
						$y =  0 -(($height-$boxHeight) / 2);	
					}			
					$smallerImage = imagecreatetruecolor($boxWidth, $boxHeight);
				}
				else
				{	
					if($newWidth < $size[0] || $newHeight < $size[0])
					{
						$width = $size[0]*$percent;
						$height = $size[1]*$percent;
					}
					else
					{
						$width = $size[0];
						$height = $size[1];	
					}
					$smallerImage = imagecreatetruecolor($width, $height);	
				}
									
				if($type == "image/png")
				{
					$source = imagecreatefrompng($origin);
					imagecopyresampled($smallerImage, $source, $x, $y, 0, 0, $width, $height, $size[0], $size[1]); 
					imagepng($smallerImage,$path);
				}
				if($type == "image/jpeg")
				{
					$source = imagecreatefromjpeg($origin);
					imagecopyresampled($smallerImage, $source, $x, $y, 0, 0, $width, $height, $size[0], $size[1]); 
					imagejpeg($smallerImage,$path);
				}
				if($type == "image/gif")
				{
					$source = imagecreatefromgif($origin);
					imagecopyresampled($smallerImage, $source, $x, $y, 0, 0, $width, $height, $size[0], $size[1]); 
					imagegif($smallerImage,$path);
				}
			}
		}
	}

	public function clean_img_name($name) {
		$char = array_merge(array_map('chr', range(0,31)),array("<", ">", ":", '"', "/", "\\", "|", "?", "*"," "));
		$name = str_replace($char, "", $name);
		$name = strtotime('now').$name;
		return $name;
	}
	
	public function house_image($id,$image){
		$query = mysql_query("INSERT INTO tbl_gallery VALUES(NULL,'".$id."','".$image."')");	
	}
	
	public function house_thumb($id,$image){
		$query = mysql_query("UPDATE tbl_house SET house_img='".$image."' WHERE house_id='".$id."'");	
	}
	
}
?>