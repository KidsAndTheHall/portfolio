<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function create_link($text) { 
	$text = str_replace("-", "&dash&", $text);
    $text = str_replace(" ", "-", $text);
    $text = str_replace("&dash&", "&-&", $text);
    $text = strtolower($text);
    $text = urlencode($text);
    return $text;
}

function decode_link($text) {
	$text = str_replace("%26-%26","&dash&",$text);
	$text = str_replace("-", " ", $text);
	$text = str_replace("&dash&", "-", $text);
	return rawurldecode($text);
}

function de_slug($text) { 
    $text = str_replace("-", " ", $text);
    return $text;
}

function check_active($active="",$instance) {
	if($active == $instance) {
		return "class='active'";
	}
}

function trim_text($length,$text) {
	if(strlen($text) > $length) {
		$text = substr($text, 0, strrpos(substr($text, 0, $length), ' '))."...";
	}
	return $text;
}

function alter_offset($offset,$rows) {
	if($offset == 1 || $offset == 0) {
		$offset = 0;
	} else {
		$offset = ($offset-1)*$rows;
	}
	return $offset;
}

function get_active_class($a,$b) {
	if($a == $b) {
		return " class=\"active\"";
	}
} 

function build_pager($base,$pages,$offset) {
	$markup = "
	<div class=\"pager\">
		<ul>";
			$con = 0;
			if($pages/5 > 1) {
				while($con<=($pages/5)) {
					if($con==0) {
						$markup .= "
							<li>
								<a href=\"".base_url($base)."\"".get_active_class($offset,$con).">".($con+1)."</a>
							</li>
						";
					} else {
						$markup .= "
							<li>
								<a href=\"".base_url($base.($con+1))."\"".get_active_class($offset,$con+1).">".($con+1)."</a>
							</li>
						";
					}
					$con++;
				}
			}
		$markup .= "</ul>
	</div>";
	return $markup;
}