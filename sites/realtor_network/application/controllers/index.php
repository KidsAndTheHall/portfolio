<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class index extends CI_Controller {
	
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$data['title']="Realtor Network";
		$this->load->model('house_model');
		$this->load->model('city_model');
		$data['query'] = $this->city_model->cities();
		$data['js_library'] = [base_url("assets/js/smoothscroll.js"),"//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"];
		$this->load->view('templates/header',$data);
		$this->load->view('pages/index',$data);
	}
	
	public function houses($city){
		$this->load->model('house_model');
		$data['array']=$this->house_model->houses_array($price1=0,$price2=1000000,$bedroom1=1,$bedroom2=10,$bathroom1=1,$bathroom2=10,$city);
		$this->load->view('pages/array',$data);
	}
	
	public function ajax_houses(){
		$this->load->model('house_model');
		$city = preg_split("/,/", $_POST['city']);
		$data['array']=$this->house_model->houses_array($_POST['price1'],$_POST['price2'],$_POST['bedroom1'],$_POST['bedroom2'],$_POST['bathroom1'],$_POST['bathroom2'],$city[0]);	
		$this->load->view('pages/array',$data);
	}
	
	public function house_array($city,$house){
		$this->load->model('house_model');
		$data['array']=$this->house_model->house_array($city,$house);	
		$this->load->view('pages/array',$data);
	}
	
	public function property($city,$address){
		$this->load->model('house_model');
		$row = $this->house_model->house($city,$address);
		$data['js_library'] = [base_url("assets/js/lightbox-2.6.min.js"),base_url("assets/libraries/flexslider/jquery.flexslider-min.js")];
		$data['css_library'] = [base_url("assets/css/lightbox.css"),base_url("assets/libraries/flexslider/jquery.flexslider-min.js")];
		$data['title']="Realtor Network &bull; ".$row['house_address'];
		$data['row'] = $row;
		$data['gallery'] = $this->house_model->gallery($row['house_id']);
		$this->load->view('templates/header',$data);
		$this->load->view('pages/house',$data);	
	}
	
	public function create(){
		$this->load->model('house_model');	
		$this->load->model('image_model');	
		$id = $this->house_model->create();
		
		foreach($_FILES['image']['name'] as $key => $value)
		{
				$temp = $_FILES["image"]["tmp_name"][$key];
				$name = $this->image_model->clean_img_name($_FILES["image"]["name"][$key]);	
				$type = $_FILES["image"]["type"][$key];	
				$origin = "../realtor_network/assets/images/houses/original/".$name;

				if(move_uploaded_file($temp, $origin))
				{
				  	$this->image_model->resize($origin, $type, "../realtor_network/assets/images/houses/250x164/".$name, 250, 164, 250, 164);
					$this->image_model->resize($origin, $type, "../realtor_network/assets/images/houses/175x115/".$name, 175, 115, 175, 115);
					$this->image_model->resize($origin, $type, "../realtor_network/assets/images/houses/77x77/".$name, 77, 77, 77, 77);
					$this->image_model->resize($origin, $type, "../realtor_network/assets/images/houses/740x335/".$name, 740, 335, 740, 335);
					$this->image_model->resize($origin, $type, "../realtor_network/assets/images/houses/lightbox/".$name, 1140, 780, 0, 0);
					$this->image_model->house_image($id,$name);
					if($key == 0){$this->image_model->house_thumb($id,$name);}
				}
		}
	}
	
	public function create_form(){
		$data['title'] = "Create";	
		$this->load->view('templates/header',$data);
		$this->load->view('pages/form',$data);		
	}
	
	public function resize_again(){
		 $query = mysql_query("SELECT gallery_image FROM tbl_gallery WHERE gallery_id > 77");
		 $this->load->model('image_model');
		 while($row = mysql_fetch_array($query)){
		 $origin = "../realtor_network/assets/images/houses/original/".$row['gallery_image'];
		 $this->image_model->resize($origin, "image/jpeg", "../realtor_network/assets/images/houses/740x335/".$row['gallery_image'], 740, 335, 740, 335);
		 }
	}
	
}
?>