<div class="container">
    <section>
        <h2 class="hidden">Interactive Map</h2>
        <div id="interactive-map">
    
        </div>
        <div id="searchColumn">
    	    <div class="searchBox">
                <form id="form">
            		<div class="search-title">
                        Search Properties
                    </div>

                    <div id="box-1">
                    	City <div id="plus-1" class="plus">+</div>
                    </div>
                    <div id="select-1">
                        <select name="city" class="inputFull" id="city">
                            <option value="1,45.421530, -75.697193">Ottawa</option>
                            <option value="4,49.22, -122.85">Vancouver</option>
                        <?php
            				while($row = mysql_fetch_array($query)) {
            				    //echo "<option value=\"".$row['city_id'].",".$row['city_lat'].", ".$row['city_long']."\">".$row['city_name']."</option>";	
            				}
            			?>
                        </select>
                    </div>

                    <div id="box-2">
                    	Price Range <div id="plus-2" class="plus">+</div>
                    </div>
                    <div id="select-2">
                        <select name="price1" class="inputLeft">
                        	<?php
            				$con = 0;
            				while($con<=1000000) {
                				echo "<option value=\"".$con."\">$".number_format($con)."</option>";
                				$con = $con+50000;	
            				}
            				?>
                        </select>
                        <div class="to">
                            to
                        </div>
                        <select name="price2" class="inputRight">
                        	<?php
                				$con = 1000000;
                				while($con>=0) {
                    				echo "<option value=\"".$con."\">$".number_format($con)."</option>";
                    				$con = $con-50000;	
                				}
            				?>
                        </select>
                    </div>

                    <div id="box-3">
                    	Number of Bedrooms <div id="plus-3" class="plus">+</div>
                    </div>
                    <div id="select-3">
                        <select name="bedroom1" class="inputLeft">
                        	<?php
                				$con = 1;
                				while($con<=10) {
                    				echo "<option value=\"".$con."\">".$con." Bedrooms</option>";
                    				$con++;	
                				}
            				?>
                        </select>
                        <div class="to">
                            to
                        </div>
                        <select name="bedroom2" class="inputRight">
                        	<?php
                				$con = 10;
                				while($con>=1) {
                    				echo "<option value=\"".$con."\">".$con." Bedrooms</option>";
                    				$con--;	
                				}
            				?>
                        </select>
                    </div>

                    <div id="box-4">
                    	Number of Bathrooms <div id="plus-4" class="plus">+</div>
                    </div>
                    <div id="select-4">
                        <select name="bathroom1" class="inputLeft">
                        	<?php
            				$con = 1;
            				while($con<=10) {
                				echo "<option value=\"".$con."\">".$con." Bathrooms</option>";
                				$con++;	
            				}
            				?>
                        </select>
                        <div class="to">
                            to
                        </div>
                        <select name="bathroom2" class="inputRight">
                        	<?php
            				$con = 10;
            				while($con>=1) {
                				echo "<option value=\"".$con."\">".$con." Bathrooms</option>";
                				$con--;	
            				}
            				?>
                        </select>
                    </div>
                    <input type="submit" class="submit" value="Search"> 
                </form>
    	    </div>
        </div>
    </section>
    
    <section>
        <h2 class="hidden">Available Homes</h2>
        <div id="properties">
          
    	</div>
    </section>

    <footer>
         <h2 class="hidden">
            Footer
         </h2>
        	<ul>
            	<li><a href="#">Home</a></li>
                <li><a href="#">Profile</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Legal</a></li>
                <li><a href="#">User Agreement</a></li>
                <li><a href="#">Privacy Policy</a></li>
            </ul>
    </footer>
</div>

    <!--<div id="arrow">
        	<a href="#top"><img src="<?php echo base_url("assets/images/structure/arrow.svg") ?>" alt="To Top" title="To Top" /></a>
    </div>-->
    </body>
</html>