<div class="container">
	<div class="leftColumn">
    	<div class="houseTitle">
            <?php echo $row['house_address'] ?> 
        </div>
    	<div class="houseImg flexslider">
            <ul class="slides">
            <?php
                $imgArray = array();
                while($img = mysql_fetch_array($gallery)){
                    echo "<li><img src=\"".base_url("assets/images/houses/740x335/".$img['gallery_image'])."\" /></li>";
                    array_push($imgArray,$img['gallery_image']);
                }
            ?>
        </ul>
        </div>
        <div class="house-icons">
            <div class="icon-box">
                <img src="<?php echo base_url("assets/images/icons/open.png") ?>" />
                <div class="icon-text">
                    Open House: <?php print date("M j, Y",strtotime($row['house_open'])); ?>
                 </div>   
            </div>    
            <div class="icon-box">
                <img src="<?php echo base_url("assets/images/icons/dollar.png") ?>" />
                <div class="icon-text">
                    Price: $<?php print number_format($row['house_price']); ?>
                 </div>   
            </div> 
            <div class="icon-box">
                <img src="<?php echo base_url("assets/images/icons/bathroom.png") ?>" />
                <div class="icon-text">
                    <?php print $row['house_bathroom']; ?> Bathrooms
                 </div>   
            </div> 
            <div class="icon-box">
                <img src="<?php echo base_url("assets/images/icons/bedroom.png") ?>" />
                <div class="icon-text">
                    <?php print $row['house_bedroom']; ?> Bedrooms
                 </div>   
            </div> 
        </div>    
        <div class="houseText">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            <br><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

        </div>
   </div>
   <div class="rightColumn">
   		 <div class="sectionTitle">
            Interactive Map
        </div>
        <div id="mini-interactive-map">
        
        </div>
        <div class="sectionTitle">
        Gallery
        </div>
        <div class="gallery">
        	<?php
			$con = 0;
			error_log($imgArray[0]);
			for($i=0; $i<sizeof($imgArray); $i++){
			if($con < 3)
			{	
			echo "<div class=\"galleryImg\"><a href=\"".base_url("assets/images/houses/lightbox/".$imgArray[$i])."\" data-lightbox=\"gallery\" title=\"".$row['house_address']."\"><img src=\"".base_url("assets/images/houses/77x77/".$imgArray[$i])."\" /></a></div>";
			$con++;
			}
			else
			{
			echo "<div class=\"galleryImg2\"><a href=\"".base_url("assets/images/houses/lightbox/".$imgArray[$i])."\" data-lightbox=\"gallery\" title=\"".$row['house_address']."\"><img src=\"".base_url("assets/images/houses/77x77/".$imgArray[$i])."\" /></a></div>";
			$con = 0;	
			}
			}
			?>
        </div>
        <div class="sectionTitle">
        Contact Realtor
        </div>
        <div class="contact-form">
        	<form>
            	<input type="text" placeholder="Your Name">
                <input type="email" placeholder="Your Email">
                <textarea placeholder="Write Your Message"></textarea>
                <input type="submit" value="Send" id="submit">
            </form>
        </div>
   </div>
     <footer>
     <h2 class="hidden">
     Footer
     </h2>
        	<ul>
            	<li><a href="#">Home</a></li>
                <li><a href="#">Profile</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Legal</a></li>
                <li><a href="#">User Agreement</a></li>
                <li><a href="#">Privacy Policy</a></li>
            </ul>
        </footer>
</div>
</body>
</html>

