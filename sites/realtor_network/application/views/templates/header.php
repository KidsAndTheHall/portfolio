<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width" />
<title><?php echo $title ?></title>

<link href="<?php echo base_url("assets/css/reset.css"); ?>" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Maven+Pro:400,700,500' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url("assets/css/styles.css"); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url("assets/images/structure/favicon.ico") ?>" rel="shortcut icon" type="image/x-icon" />
<?php 
    if(isset($css_library)) {
        for($i=0; $i<sizeof($css_library); $i++) {
            echo "<link href=\"".$css_library[$i]."\" rel='stylesheet' type='text/css'>";
        }
    }
?>


<script src="<?php echo base_url("assets/js/json-minified.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.js"); ?>"></script> 
<script type="text/javascript" src="<?php echo base_url("assets/js/javascript.js"); ?>"></script> 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?AIzaSyCw5bjejXoH-6Lc7s7jQy9-vlL4jqQcejM&sensor=false"></script> 
<?php 
    if(isset($js_library)) {
        for($i=0; $i<sizeof($js_library); $i++) {
            echo "<script type=\"text/javascript\" src=\"".$js_library[$i]."\"></script>";
        }
    }
?>

</head>

<body>
<a name="top"></a>
<header>
<h1 class="hidden">Realtors Network</h1>
	<nav>
    <h2 class="hidden">Main Navigation</h2>
    	<div class="nav-bar">
        	<div class="nav-contain">
           		<div class="logo">
                	<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url("assets/images/structure/logo.svg") ?>" alt="Realtor Network" title="Realtor Network"/></a>
                </div>
                <div class="sign-in">
                    <a href="#">Sign In or Create an Account</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="nav-fix"></div>
</header>
